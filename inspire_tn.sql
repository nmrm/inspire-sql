CREATE DATABASE inspiredb;
\connect inspiredb;
CREATE SCHEMA inspire;
SET SCHEMA 'inspire';
CREATE EXTENSION postgis;

--
--CODE LISTs
--

CREATE TYPE GeometrySpecificationValue AS ENUM (
	'postalDelivery',
	'utilityService',
	'thoroughfareAccess',
	'entrance',
	'building',
	'parcel',
	'segment',
	'postalDescriptor',
	'addressArea',
	'adminUnit1stOrder',
	'adminUnit2ndOrder',
	'adminUnit3rdOrder',
	'adminUnit4thOrder',
	'adminUnit5thOrder',
	'adminUnit6thOrder'
);

CREATE TYPE GeometryMethodValue AS ENUM (
    'fromFeature',
    'byAdministrator',
    'byOtherParty'
);

CREATE TYPE VoidReasonValue AS ENUM (
    'Unpopulated',
    'Unknown',
    'Withheld'
);

CREATE TYPE StatusValue AS ENUM (
    'current',
    'retired',
    'proposed',
    'reserved',
    'alternative'
);

CREATE TYPE LocatorLevelValue AS ENUM (
    'siteLevel',
    'accessLevel',
    'unitLevel',
    'postalDeliveryPoint'
);

CREATE TYPE LocatorDesignatorTypeValue AS ENUM (
    'addressIdentifierGeneral',
    'addressNumber',
    'addressNumberExtension',
    'addressNumber2ndExtension',
    'buildingIdentifier',
    'buildingIdentifierPrefix',
    'entranceDoorIdentifier',
    'staircaseIdentifier',
    'floorIdentifier',
    'unitIdentifier',
    'postalDeliveryIdentifier',
    'kilometrePoint',
    'cornerAddress1stIdentifier',
    'cornerAddress2ndIdentifier'
);

CREATE TYPE LocatorNameTypeValue AS ENUM (
    'siteName',
    'buildingName',
    'roomName',
    'descriptiveLocator'
);

CREATE TYPE NativenessValue AS ENUM (
    'endonym',
    'exonym'
);

CREATE TYPE NameSatusValue AS ENUM (
    'official',
    'standardised',
    'historical',
    'other'
);

CREATE TYPE GrammaticalGenderValue AS ENUM (
    'masculine',
    'feminine',
    'neuter',
    'common'
);

CREATE TYPE GrammaticalNumberValue AS ENUM (
    'singular',
    'plural',
    'dual'
);

CREATE TYPE NamedPlaceTypeValue AS ENUM (
    'administrativeUnit',
    'building',
    'hydography',
    'landcover',
    'landform',
    'populatedPlace',
    'protectedSite',
    'transportNetwork',
    'other'
);

CREATE TYPE AerodromeCategoryValue AS ENUM (
    'domesticNational',
    'domesticRegional',
    'international'
);

CREATE TYPE SpokeTypeValue AS ENUM (
    'start',
    'end'
);

CREATE TYPE AerodromeTypeValue AS ENUM (
    'aerodromeHeliport',
    'aerodromeOnly',
    'heliportOnly',
    'landingSite'
);

CREATE TYPE AirRouteTypeValue AS ENUM (
    'ATS',
    'NAT'
);

CREATE TYPE AirRouteLinkClassValue AS ENUM (
    'convetional',
    'RNAV',
    'TACAN'
);

CREATE TYPE AirspaceAreaTypeValue AS ENUM (
    'ATZ',
    'CTA',
    'CTR',
    'D',
    'FIR',
    'P',
    'TMA',
    'UIR'
);

CREATE TYPE ConditionOfFacilityValue AS ENUM (
    'disused',
    'functional',
    'projected',
    'underConstruction',
    'decommissioned'
);

CREATE TYPE RunwayTypeValue AS ENUM (
    'runway',
    'FATO'
);


CREATE TYPE NavaidTypeValue AS ENUM (
    'DME',
    'ILS',
    'ILS-DME',
    'LOC',
    'LOC-DME',
    'MKR',
    'MLS',
    'MLS-DME',
    'NDB',
    'NDB-DME',
    'NDM-MKR',
    'TACAN',
    'TLS',
    'VOR',
    'VOR-DME',
    'VORTAC'
);

CREATE TYPE PointRoleValue AS ENUM (
    'end',
    'mid',
    'start',
    'threshold'
);

CREATE TYPE SurfaceCompositionValue AS ENUM (
    'asphal',
    'concrete',
    'grass'
);

CREATE TYPE AirUseRestrictionValue AS ENUM (
    'reservedForMilitary',
    'temporalRestrictions'
);

CREATE TYPE TransportTypeValue AS ENUM (
    'air',
    'cable',
    'rail',
    'road',
    'water'
);

--
--InspireID instrumentation to allow constraints checking and uniqueness across features
--

CREATE TABLE bt_identifier (
    ogc_fid SERIAL PRIMARY KEY,
    localId character varying NOT NULL, -- CHECK('[A-Za-z0-9_\.-]'),
    namespace character varying NOT NULL, -- CHECK('[A-Za-z0-9_\.-]'),
    versionId character varying(25), -- CHECK('[A-Za-z0-9_\.-]'),
    versionId_void VoidReasonValue,
    inspireid character varying UNIQUE,
    CHECK (versionId IS NOT null OR versionId_void IS NOT null),
    CHECK (localId ~* '[A-Za-z0-9_\.-]'),
    CHECK (namespace ~*'[A-Za-z0-9_\.-]'),
    CHECK (versionId ~*'[A-Za-z0-9_\.-]')
);

CREATE OR REPLACE FUNCTION inspireid_create() RETURNS trigger AS '
   BEGIN
        NEW.inspireid := NEW.localId||NEW.namespace||New.versionId;
        RETURN NEW;
   END' LANGUAGE plpgsql;

CREATE TRIGGER inspireid_create BEFORE INSERT OR UPDATE ON bt_identifier FOR EACH ROW EXECUTE PROCEDURE inspireid_create();

CREATE UNIQUE INDEX inspireid_idx ON bt_identifier (localId, namespace, versionId);

--
--LocalizedCharacterStringType
--We could create a code list with the valid locales
CREATE DOMAIN locale_type AS character varying(5);

CREATE TABLE localizedCharacterStringType (
    ogc_fid SERIAL PRIMARY KEY,
    locale locale_type,
    text character varying
);

--
---GeographicalNames Theme--
--
CREATE TABLE gn_namedplace_utm25 (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Geometry,5015), --from specs this could be any kind of geometry
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT null,
    leastdetailedviewingscale integer, --restricts the scale to only integer numbers
    leastdetailedviewingscale_void VoidReasonValue,
    localtype boolean,
    localtype_void VoidReasonValue,
    mostdetailedviewingscale integer, --restricts the scale to only integer numbers
    mostdetailedviewingscale_void VoidReasonValue,
    relatedspacialobject boolean,
    relatedspacialobject_void VoidReasonValue,
    type boolean,
    type_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (leastdetailedviewingscale IS NOT null OR leastdetailedviewingscale_void IS NOT null),
    CHECK (localtype IS NOT null OR localtype_void IS NOT null),
    CHECK (mostdetailedviewingscale IS NOT null OR mostdetailedviewingscale_void IS NOT null),
    CHECK (relatedspacialobject IS NOT null OR relatedspacialobject_void IS NOT null),
    CHECK (type IS NOT null OR type_void IS NOT null)
);

CREATE TABLE gn_namedplace_utm26 (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Geometry,5014), --from specs this could be any kind of geometry
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT null,
    leastdetailedviewingscale integer, --restricts the scale to only integer numbers
    leastdetailedviewingscale_void VoidReasonValue,
    localtype boolean,
    localtype_void VoidReasonValue,
    mostdetailedviewingscale integer, --restricts the scale to only integer numbers
    mostdetailedviewingscale_void VoidReasonValue,
    relatedspacialobject boolean,
    relatedspacialobject_void VoidReasonValue,
    type boolean,
    type_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (leastdetailedviewingscale IS NOT null OR leastdetailedviewingscale_void IS NOT null),
    CHECK (localtype IS NOT null OR localtype_void IS NOT null),
    CHECK (mostdetailedviewingscale IS NOT null OR mostdetailedviewingscale_void IS NOT null),
    CHECK (relatedspacialobject IS NOT null OR relatedspacialobject_void IS NOT null),
    CHECK (type IS NOT null OR type_void IS NOT null)
);

--The relationship between gn_geographicalname and gn_namedplace should be many-to-many to allow account for different spellings, nonetheless to avoid complexiness for now is implemented as 1 to many

CREATE TABLE gn_geographicalname (
    ogc_fid SERIAL PRIMARY KEY,
    language character varying(3),
    language_void VoidReasonValue,
    nativeness NativenessValue,
    nativeness_void VoidReasonValue,
    namestatus NameSatusValue,
    namestatus_void VoidReasonValue,
    sourceofname character varying,
    sourceofname_void VoidReasonValue,
    grammaticalgender GrammaticalGenderValue,
    grammaticalgender_void VoidReasonValue,
    grammaticalnumber GrammaticalNumberValue,
    grammaticalnumber_void VoidReasonValue,
    pronunciation integer,
    pronunciation_void VoidReasonValue,
    spelling character varying NOT NULL UNIQUE,
    CHECK (pronunciation IS NOT null OR pronunciation_void IS NOT null),
    CHECK (sourceofname IS NOT null OR sourceofname_void IS NOT null),
    CHECK (namestatus IS NOT null OR namestatus_void IS NOT null),
    CHECK (nativeness IS NOT null OR nativeness_void IS NOT null),
    CHECK (language IS NOT null OR language_void IS NOT null)
);

CREATE TABLE gn_pronunciationofname (
    ogc_fid SERIAL PRIMARY KEY,
    pronunciationsoundlink character varying,
    pronunciationsoundlink_void VoidReasonValue,
    pronunciationpa character varying,
    pronunciationpa_void VoidReasonValue,
    CHECK (pronunciationsoundlink IS NOT null OR pronunciationpa IS NOT null),
    CHECK (pronunciationpa IS NOT null OR pronunciationpa_void IS NOT null),
    CHECK (pronunciationsoundlink IS NOT null OR pronunciationsoundlink_void IS NOT null)
);

CREATE TABLE gn_spellingofname (
    ogc_fid SERIAL PRIMARY KEY,
    text character varying NOT NULL UNIQUE,
    scrip character varying(4),
    scrip_void VoidReasonValue,
    transliterationscheme character varying,
    transliterationscheme_void VoidReasonValue,
    CHECK (transliterationscheme IS NOT null OR transliterationscheme_void IS NOT null),
    CHECK (scrip IS NOT null OR scrip_void IS NOT null)
);

--tables needed to create a many-to-many relationships

CREATE TABLE mm_gn_namedplace_utm25 (
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace integer REFERENCES gn_namedplace_utm25 (ogc_fid) ,
    id_geoname integer REFERENCES gn_geographicalname (ogc_fid)
);

CREATE TABLE mm_gn_namedplace_utm26 (
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace integer REFERENCES gn_namedplace_utm26 (ogc_fid) ,
    id_geoname integer REFERENCES gn_geographicalname (ogc_fid)
);

CREATE TABLE mm_namedplace_localtype_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace integer REFERENCES gn_namedplace_utm25 (ogc_fid) ,
    id_localtype integer REFERENCES localizedCharacterStringType (ogc_fid)
);

CREATE TABLE mm_namedplace_localtype_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace integer REFERENCES gn_namedplace_utm26 (ogc_fid) ,
    id_localtype integer REFERENCES localizedCharacterStringType (ogc_fid)
);

CREATE TABLE mm_namedplace_relatedspatialobject_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace integer REFERENCES gn_namedplace_utm25 (ogc_fid),
    id_inspireid character varying REFERENCES bt_identifier (inspireId)
);

CREATE TABLE mm_namedplace_relatedspatialobject_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace integer REFERENCES gn_namedplace_utm26 (ogc_fid),
    id_inspireid character varying REFERENCES bt_identifier (inspireId)
);

CREATE TABLE mm_namedplace_type_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace integer REFERENCES gn_namedplace_utm26 (ogc_fid),
    type NamedPlaceTypeValue
);

CREATE TABLE mm_namedplace_type_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace integer REFERENCES gn_namedplace_utm25 (ogc_fid),
    type NamedPlaceTypeValue
);

---Relationships---

ALTER TABLE gn_geographicalname ADD CONSTRAINT gntopronunfk FOREIGN KEY (pronunciation) REFERENCES gn_pronunciationofname (ogc_fid);

ALTER TABLE gn_geographicalname ADD CONSTRAINT gntospellfk FOREIGN KEY (spelling) REFERENCES gn_spellingofname (text);


---
---Common transport Elements
---

CREATE TABLE tnc_transportnetwork(
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    typeoftransport TransportTypeValue NOT null,
    geographicalname boolean,
    geographicalname_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

CREATE TABLE tnc_transportnetwork_geoname(
    ogc_fid SERIAL PRIMARY KEY,
    transportnetwork character varying REFERENCES tnc_transportnetwork (inspireid) NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling) NOT NULL
);

CREATE TABLE tnc_transportnetwork_elements(
    ogc_fid SERIAL PRIMARY KEY,
    transportnetwork character varying REFERENCES tnc_transportnetwork (inspireid) NOT NULL,
    elements character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

CREATE TABLE tnc_markerpost_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5015),
    location NUMERIC(20,2) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    route character varying REFERENCES bt_identifier (inspireId) NOT NULL, -- All linkSet have unique external identifiers
    route_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (innetwork IS NOT null OR innetwork_void IS NOT null)
);

CREATE TABLE tnc_markerpost_network_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    markerpost character varying REFERENCES tnc_markerpost_utm25 (inspireid) NOT NULL,
    network character varying REFERENCES tnc_transportnetwork (inspireid) NOT NULL
);

CREATE TABLE tnc_markerpost_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5014),
    location NUMERIC(20,2) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    route character varying REFERENCES bt_identifier (inspireId), -- All linkSet have unique external identifiers
    route_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (innetwork IS NOT null OR innetwork_void IS NOT null)
);

CREATE TABLE tnc_markerpost_network_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    markerpost character varying REFERENCES tnc_markerpost_utm26 (inspireid) NOT NULL,
    network character varying REFERENCES tnc_transportnetwork (inspireid) NOT NULL
);

--
--Air Transportation theme--
---
CREATE TABLE tn_aerodromearea_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5015),
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

CREATE TABLE tn_aerodromearea_network_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    aerodromearea integer REFERENCES tn_aerodromearea_utm25 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

CREATE TABLE tn_aerodromearea_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5014),
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

CREATE TABLE tn_aerodromearea_network_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    aerodromearea integer REFERENCES tn_aerodromearea_utm26 (ogc_fid),
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

--this property can only be associated with a spatial object that is an aerodromearea or aerdromenode
CREATE TABLE tn_aerodromecategory(
    ogc_fid SERIAL PRIMARY KEY,
    aerodromecategory AerodromeCategoryValue NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL ,
    networkref boolean,
    networkref_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_aerodromecategory_networkelement(
    ogc_fid SERIAL PRIMARY KEY,
    aerodromecategory integer REFERENCES tn_aerodromecategory (ogc_fid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL -- REVIEW
);

--- ABSTRACT
-- CREATE TABLE tn_airlink(
    -- ogc_fid SERIAL PRIMARY KEY,
    -- wkb_geometry_utm25 geometry(Multicurve,5015),
    -- wkb_geometry_utm26 geometry(Multicurve,5014),
    -- validfrom TIMESTAMP,
    -- validfrom_void VoidReasonValue,
    -- validto TIMESTAMP,
    -- validto_void VoidReasonValue,
    -- inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    -- geographicalname character varying REFERENCES gn_geographicalname (spelling),
    -- geographicalname_void VoidReasonValue,
    -- fictious boolean NOT NULL,
    -- CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    -- CHECK (validto IS NOT null OR validto_void IS NOT null),
    -- CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
-- );
-- ---
CREATE TABLE tn_aerodromenode_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5015),
    designatorIATA character varying(3),
    designatorIATA_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    locationindicatorICAO character varying(4),
    locationindicatorICAO_void  VoidReasonValue,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (designatorIATA IS NOT null OR designatorIATA_void IS NOT null),
    CHECK (locationindicatorICAO IS NOT null OR locationindicatorICAO_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_aerodromenode_spokes_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    aerodromenodeid character varying REFERENCES tn_aerodromenode_utm25 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL
);

CREATE TABLE tn_aerodromenode_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5014),
    designatorIATA character varying(3),
    designatorIATA_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    locationindicatorICAO character varying(4),
    locationindicatorICAO_void  VoidReasonValue,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (designatorIATA IS NOT null OR designatorIATA_void IS NOT null),
    CHECK (locationindicatorICAO IS NOT null OR locationindicatorICAO_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_aerodromenode_spokes_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    aerodromenodeid character varying REFERENCES tn_aerodromenode_utm26 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL
);

--Applies only to aerdromenode and aerodromearea
CREATE TABLE tn_aerodrometype(
    ogc_fid SERIAL PRIMARY KEY,
    aerodrometype AerodromeTypeValue NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_aerodrometype_networkelement(
    ogc_fid SERIAL PRIMARY KEY,
    aerodrometype integer REFERENCES tn_aerodrometype (ogc_fid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL ---Review
);


CREATE TABLE tn_airlinksequence(
    ogc_fid SERIAL PRIMARY KEY,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

--Consider creating a trigger to verify that every link in the same sequence belong to the same network

CREATE TABLE tn_airlink_sequence(
    ogc_fid SERIAL PRIMARY KEY,
    airlinksequence character varying REFERENCES tn_airlinksequence (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    linkorder integer NOT NULL,--Order of the link in the sequence.
    sign boolean NOT NULL
);


--ABSTRACT--
-- CREATE TABLE tn_airnode_utm26(
    -- ogc_fid SERIAL PRIMARY KEY,
    -- wkb_geometry geometry(Point,5014),
    -- geographicalname character varying REFERENCES gn_geographicalname (spelling),
    -- geographicalname_void VoidReasonValue,
    -- inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    -- singificantpoint boolean NOT NULL,
    -- spokeend boolean,
    -- spokeend_void VoidReasonValue,
    -- spokestart boolean,
    -- spokestart_void VoidReasonValue,
    -- validfrom TIMESTAMP,
    -- validfrom_void VoidReasonValue,
    -- validto TIMESTAMP,
    -- validto_void VoidReasonValue,
    -- CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    -- CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    -- CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    -- CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    -- CHECK (validto IS NOT null OR validto_void IS NOT null),
-- );

-- CREATE TABLE tn_airnode_spokes_utm26(
    -- ogc_fid SERIAL PRIMARY KEY,
    -- airnodeid character varying REFERENCES tn_airdnode_utm26 (inspireid) NOT NULL,
    -- networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    -- spoketype SpokeTypeValue NOT NULL
-- );

-- CREATE TABLE tn_airnode_utm25(
    -- ogc_fid SERIAL PRIMARY KEY,
    -- wkb_geometry geometry(Point,5015),
    -- geographicalname character varying REFERENCES gn_geographicalname (spelling),
    -- geographicalname_void VoidReasonValue,
    -- inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    -- singificantpoint boolean NOT NULL,
    -- spokeend boolean,
    -- spokeend_void VoidReasonValue,
    -- spokestart boolean,
    -- spokestart_void VoidReasonValue,
    -- validfrom TIMESTAMP,
    -- validfrom_void VoidReasonValue,
    -- validto TIMESTAMP,
    -- validto_void VoidReasonValue,
    -- CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    -- CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    -- CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    -- CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    -- CHECK (validto IS NOT null OR validto_void IS NOT null),
-- );

-- CREATE TABLE tn_airnode_spokes_utm25(
    -- ogc_fid SERIAL PRIMARY KEY,
    -- airnodeid character varying REFERENCES tn_airnode_utm25 (inspireid) NOT NULL,
    -- networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    -- spoketype SpokeTypeValue NOT NULL
-- );

CREATE TABLE tn_airroute(
    ogc_fid SERIAL PRIMARY KEY,
    airroutetype AirRouteTypeValue,
    airroutetype_void   VoidReasonValue,
    beginlifespanversion TIMESTAMP not NULL,
    designator character varying,
    designator_void VoidReasonValue,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    markerpost boolean,
    markerpost_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (airroutetype IS NOT null OR airroutetype_void IS NOT null),
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (innetwork IS NOT null OR innetwork_void IS NOT null),
    CHECK (markerpost IS NOT null OR markerpost_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_airroute_links (
    ogc_fid SERIAL PRIMARY KEY,
    airroute character varying REFERENCES tn_airroute (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

CREATE TABLE tn_airroute_markerposts(
    ogc_fid SERIAL PRIMARY KEY,
    airroute character varying REFERENCES tn_airroute (inspireid) NOT NULL,
    markerpost_utm25 character varying REFERENCES tnc_markerpost_utm25 (inspireid),
    markerpost_utm26 character varying REFERENCES tnc_markerpost_utm26 (inspireid),
    CHECK (markerpost_utm25 IS NOT null OR markerpost_utm26 IS NOT null)
);

CREATE TABLE tn_airroute_network(
    ogc_fid SERIAL PRIMARY KEY,
    airroute character varying REFERENCES tn_airroute (inspireid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

CREATE TABLE tn_airroutelink(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry_utm25 geometry(Multicurve,5015),
    wkb_geometry_utm26 geometry(Multicurve,5014),
    airroutelinkclass AirRouteLinkClassValue,
    airroutelinkclass_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    fictious boolean NOT NULL,
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (airroutelinkclass IS NOT null OR airroutelinkclass_void IS NOT null)
);

CREATE TABLE tn_airspacearea_utm25 (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5015),
    airspaceareatype AirspaceAreaTypeValue NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

CREATE TABLE tn_airspacearea_network_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    airspacearea integer REFERENCES tn_airspacearea_utm25 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

CREATE TABLE tn_airspacearea_utm26 (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5014),
    airspaceareatype AirspaceAreaTypeValue NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

CREATE TABLE tn_airspacearea_network_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    airspacearea integer REFERENCES tn_airspacearea_utm26 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

CREATE TABLE tn_apronarea_utm26 (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5014),
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

CREATE TABLE tn_apron_network_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    apronarea integer REFERENCES tn_apronarea_utm26 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

CREATE TABLE tn_apronarea_utm25 (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5015),
    beginlifespanversion TIMESTAMP not NULL not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

CREATE TABLE tn_apron_network_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    apronarea integer REFERENCES tn_apronarea_utm25 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

--Applies only to aerodromenode, aerodromeaerea and runwayarea
CREATE TABLE tn_conditionofairfacility(
    ogc_fid SERIAL PRIMARY KEY,
    currentstatus ConditionOfFacilityValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    beginlifespanversion TIMESTAMP not NULL not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_cofacility_networkref(
    ogc_fid SERIAL PRIMARY KEY,
    cofacilityid character REFERENCES tn_conditionofairfacility (inspireid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

CREATE TABLE tn_designatedpoint_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5015),
    designator character varying,
    designator_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_designatedpoint_spokes_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    designatedpointid character varying REFERENCES tn_designatedpoint_utm25 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL
);

CREATE TABLE tn_designatedpoint_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5014),
    designator character varying,
    designator_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_designatedpoint_spokes_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    designatedpointid character varying REFERENCES tn_designatedpoint_utm26 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL
);

--Applies only to RunwayArea TaxywayArea and TouchDownliftof
CREATE TABLE tn_elementlength(
    ogc_fid SERIAL PRIMARY KEY,
    measure NUMERIC(20,2) NOT NULL, --Should be enough for most measures
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    beginlifespanversion TIMESTAMP not NULL not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_elementlength_networkref(
    ogc_fid SERIAL PRIMARY KEY,
    elementlengthid character varying REFERENCES tn_elementlength (inspireid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

--Applies only to RunwayArea TaxywayArea and TouchDownliftof
CREATE TABLE tn_elementwidth(
    ogc_fid SERIAL PRIMARY KEY,
    width NUMERIC(20,2) NOT NULL, --Should be enough for most measures
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_elementwidth_networkref(
    ogc_fid SERIAL PRIMARY KEY,
    elementwidthid character varying REFERENCES tn_elementwidth (inspireid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

--Applies only to Aerodrome Node and AerodromeArea - is this difference between the highest point of the landing area and the mean sea level
CREATE TABLE tn_fieldelevation(
    ogc_fid SERIAL PRIMARY KEY,
    elevation NUMERIC(20,2) NOT NULL, --Should be enough for most measures
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_fieldelevation_networkref(
    ogc_fid SERIAL PRIMARY KEY,
    fieldelevationid character varying REFERENCES tn_fieldelevation (inspireid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

CREATE TABLE tn_instrumentapproachprocedure(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry_utm25 geometry(Multicurve,5015),
    wkb_geometry_utm26 geometry(Multicurve,5014),
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    fictious boolean NOT NULL,
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null)
);

CREATE TABLE tn_runwayarea_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5015),
    designator character varying,
    designator_void VoidReasonValue,
    runwaytype RunwayTypeValue,
    runwaytype_void VoidReasonValue,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null),
    CHECK (runwaytype IS NOT null OR runwaytype_void IS NOT null)
);

CREATE TABLE tn_runwayarea_network_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    runwayareaid integer REFERENCES tn_runwayarea_utm25 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

CREATE TABLE tn_runwayarea_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5014),
    designator character varying,
    designator_void VoidReasonValue,
    runwaytype RunwayTypeValue,
    runwaytype_void VoidReasonValue,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null),
    CHECK (runwaytype IS NOT null OR runwaytype_void IS NOT null)
);

CREATE TABLE tn_runwayarea_network_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    runwayareaid integer REFERENCES tn_runwayarea_utm26 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

--Applies only to Air Route Link and AirspaceArea
CREATE TABLE tn_loweraltitudelimit(
    ogc_fid SERIAL PRIMARY KEY,
    altitude NUMERIC(20,2)  NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_loweraltitudelimit_networkref(
    ogc_fid SERIAL PRIMARY KEY,
    loweraltitudelimitid character varying REFERENCES tn_loweraltitudelimit (inspireid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

CREATE TABLE tn_navaid_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5014),
    navaidtype NavaidTypeValue,
    navaidtype_void VoidReasonValue,
    designator character varying,
    designator_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (navaidtype IS NOT null OR navaidtype_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null)
);

CREATE TABLE tn_navaid_spokes_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    navaidid character varying REFERENCES tn_navaid_utm26 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL
);

CREATE TABLE tn_navaid_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5015),
    navaidtype NavaidTypeValue,
    navaidtype_void VoidReasonValue,
    designator character varying,
    designator_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (navaidtype IS NOT null OR navaidtype_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null)
);

CREATE TABLE tn_navaid_spokes_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    navaidid character varying REFERENCES tn_navaid_utm25 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL,
    sign boolean
);

CREATE TABLE tn_runwaycenterlinepoint_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5014),
    pointrole PointRoleValue NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_runwaycenterlinepoint_spokes_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    centerlinepointid character varying REFERENCES tn_runwaycenterlinepoint_utm26 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL,
    sign boolean
);

CREATE TABLE tn_runwaycenterlinepoint_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5015),
    pointrole PointRoleValue NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_runwaycenterlinepoint_spokes_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    centerlinepointid character varying REFERENCES tn_runwaycenterlinepoint_utm25 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL
);

CREATE TABLE tn_standardinstrumentarrival(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry_utm25 geometry(Multicurve,5015),
    wkb_geometry_utm26 geometry(Multicurve,5014),
    designator character varying,
    designator_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    fictious boolean NOT NULL,
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null)
);

CREATE TABLE tn_standardinstrumentdeparture(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry_utm25 geometry(Multicurve,5015),
    wkb_geometry_utm26 geometry(Multicurve,5014),
    designator character varying,
    designator_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    fictious boolean NOT NULL,
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null)
);

CREATE TABLE tn_procedurelink(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry_utm25 geometry(Multicurve,5015),
    wkb_geometry_utm26 geometry(Multicurve,5014),
    designator character varying,
    designator_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    fictious boolean NOT NULL,
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null)
);


--Applies only to RunwayArea, TaxywayArea,  Apronarea and touchdownliftoff
CREATE TABLE tn_surfacecomposition(
    ogc_fid SERIAL PRIMARY KEY,
    surfacecomposition SurfaceCompositionValue NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_surfacecomposition_networkref(
    ogc_fid SERIAL PRIMARY KEY,
    surfacecompositionid character varying REFERENCES tn_surfacecomposition (inspireid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

CREATE TABLE tn_taxiwayarea_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5014),
    designator character varying,
    designator_void VoidReasonValue,
    runwaytype RunwayTypeValue,
    runwaytype_void VoidReasonValue,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null),
    CHECK (runwaytype IS NOT null OR runwaytype_void IS NOT null)
);

CREATE TABLE tn_taxiwayarea_network_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    runwayareaid integer REFERENCES tn_taxiwayarea_utm26 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

CREATE TABLE tn_taxiwayarea_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Polygon,5015),
    designator character varying,
    designator_void VoidReasonValue,
    runwaytype RunwayTypeValue,
    runwaytype_void VoidReasonValue,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    innetwork boolean,
    innetwork_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (designator IS NOT null OR designator_void IS NOT null),
    CHECK (runwaytype IS NOT null OR runwaytype_void IS NOT null)
);

CREATE TABLE tn_taxiwayarea_network_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    runwayareaid integer REFERENCES tn_taxiwayarea_utm26 (ogc_fid) NOT NULL,
    network integer REFERENCES tnc_transportnetwork (ogc_fid) NOT NULL
);

CREATE TABLE tn_touchdownliftoff_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5015),
    designator character varying,
    designator_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_touchdownliftoff_spokes_utm25(
    ogc_fid SERIAL PRIMARY KEY,
    touchdownliftoffid character varying REFERENCES tn_touchdownliftoff_utm25 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL,
    sign boolean
);

CREATE TABLE tn_touchdownliftoff_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,5014),
    designator character varying,
    designator_void VoidReasonValue,
    geographicalname character varying REFERENCES gn_geographicalname (spelling),
    geographicalname_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    singificantpoint boolean NOT NULL,
    spokeend boolean,
    spokeend_void VoidReasonValue,
    spokestart boolean,
    spokestart_void VoidReasonValue,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    CHECK (geographicalname IS NOT null OR geographicalname_void IS NOT null),
    CHECK (spokeend IS NOT null OR spokeend_void IS NOT null),
    CHECK (spokestart IS NOT null OR spokestart_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null)
);

CREATE TABLE tn_touchdownliftoff_spokes_utm26(
    ogc_fid SERIAL PRIMARY KEY,
    touchdownliftoffid character varying REFERENCES tn_touchdownliftoff_utm26 (inspireid) NOT NULL,
    networklinkid character varying REFERENCES bt_identifier (inspireId) NOT NULL,
    spoketype SpokeTypeValue NOT NULL,
    sign boolean
);

--Applies only to airoute link and airspace area
CREATE TABLE tn_upperaltitudelimit(
    ogc_fid SERIAL PRIMARY KEY,
    altitude NUMERIC(20,2) NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_upperaltitudelimit_networkref(
    ogc_fid SERIAL PRIMARY KEY,
    upperaltitudelimitid character varying REFERENCES tn_upperaltitudelimit (inspireid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL
);

-- Applies only to Air Route Air links Air nodes and AerodromeArea

CREATE TABLE tn_userestriction(
    ogc_fid SERIAL PRIMARY KEY,
    restriction AirUseRestrictionValue NOT NULL,
    validfrom TIMESTAMP,
    validfrom_void VoidReasonValue,
    validto TIMESTAMP,
    validto_void VoidReasonValue,
    inspireid character varying REFERENCES bt_identifier (inspireId) UNIQUE NOT NULL,
    beginlifespanversion TIMESTAMP not NULL,
    endlifespanversion TIMESTAMP,
    endlifespanversion_void VoidReasonValue,
    networkref boolean,
    networkref_void VoidReasonValue,
    CHECK (endlifespanversion IS NOT null OR endlifespanversion_void IS NOT null),
    CHECK (validfrom IS NOT null OR validfrom_void IS NOT null),
    CHECK (validto IS NOT null OR validto_void IS NOT null),
    CHECK (networkref IS NOT null OR networkref_void IS NOT null)
);

CREATE TABLE tn_userestriction_networkref(
    ogc_fid SERIAL PRIMARY KEY,
    userestrictionid character varying REFERENCES tn_userestriction (inspireid) NOT NULL,
    networkelement character varying REFERENCES bt_identifier (inspireId) NOT NULL
);
--
