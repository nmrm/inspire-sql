SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE SCHEMA topology;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';

CREATE EXTENSION IF NOT EXISTS postgis_topology WITH SCHEMA topology;

COMMENT ON EXTENSION postgis_topology IS 'PostGIS topology spatial types and functions';

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE TABLE ac_mfgridobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_observation character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ac_mfgridobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_observation character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ac_mfgridseriesobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_observation character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ac_mfgridseriesobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_observation character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ac_mfmultipointobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPoint,900915),
    id_observation character varying
);

CREATE TABLE ac_mfmultipointobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPoint,900914),
    id_observation character varying
);

CREATE TABLE ac_mfpointobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_observation character varying
);

CREATE TABLE ac_mfpointobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_observation character varying
);

CREATE TABLE ac_mfpointtimeseriesobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_observation character varying
);

CREATE TABLE ac_mfpointtimeseriesobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_observation character varying
);

CREATE TABLE ac_mfsamplingcurve_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    id_samplingcurve character varying,
    shape_length double precision
);

CREATE TABLE ac_mfsamplingcurve_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    id_samplingcurve character varying,
    shape_length double precision
);

CREATE TABLE ac_mfsamplingpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_samplingpoint character varying
);

CREATE TABLE ac_mfsamplingpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_samplingpoint character varying
);

CREATE TABLE ac_mfsamplingsolid_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    id_samplingsolid character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ac_mfsamplingsolid_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    id_samplingsolid character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ac_mfsamplingsurface_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_samplingsurface character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ac_mfsamplingsurface_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_samplingsurface character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ac_mftrajectoryobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    id_observation character varying,
    shape_length double precision
);

CREATE TABLE ac_mftrajectoryobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    id_observation character varying,
    shape_length double precision
);

CREATE TABLE ad_addressgeographicalposition_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    default_ boolean,
    specification geometrySpecificationValue,
    specification_void character varying,
    method geometryMethodValue,
    method_void character varying,
    id_address character varying
);

CREATE TYPE geometrySpecificationValue AS ENUM (
	'postalDelivery',
	'utilityService',
	'thoroughfareAccess',
	'entrance',
	'building',
	'parcel',
	'segment',
	'postalDescriptor',
	'addressArea',
	'adminUnit1stOrder',
	'adminUnit2ndOrder',
	'adminUnit3rdOrder',
	'adminUnit4thOrder',
	'adminUnit5thOrder',
	'adminUnit6thOrder'
);

CREATE TYPE geometryMethodValue AS ENUM (
    'fromFeature',
    'byAdministrator',
    'byOtherParty'
);

CREATE TABLE ad_addressgeographicalposition_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    default_ character varying,
    specification geometrySpecificationValue,
    specification_void character varying,
    method character varying,
    method_cl character varying,
    method_void character varying,
    id_address character varying
);

CREATE TABLE afholdingpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900915),
    inspireid character varying,
    contains character varying
);

CREATE TABLE afholdingpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900914),
    inspireid character varying,
    contains character varying
);

CREATE TABLE afholdingpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    contains character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE afholdingpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    contains character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE afsitepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900915),
    includesanimal_void character varying,
    id_site character varying,
    id_holding character varying
);

CREATE TABLE afsitepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900914),
    includesanimal_void character varying,
    id_site character varying,
    id_holding character varying
);

CREATE TABLE afsitepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    includesanimal_void character varying,
    id_site character varying,
    id_holding character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE afsitepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    includesanimal_void character varying,
    id_site character varying,
    id_holding character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ammngmtrestrictionorregzoneline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    thematicid_void character varying,
    name_void character varying,
    specialisedzonetype character varying,
    specialisedzonetype_cl character varying,
    specialisedzonetype_void character varying,
    designationperiod_begin timestamp with time zone,
    designationperiod_end timestamp with time zone,
    designationperiod_void character varying,
    competentauthority_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedzone integer,
    relatedzone_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE ammngmtrestrictionorregzoneline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    thematicid_void character varying,
    name_void character varying,
    specialisedzonetype character varying,
    specialisedzonetype_cl character varying,
    specialisedzonetype_void character varying,
    designationperiod_begin timestamp with time zone,
    designationperiod_end timestamp with time zone,
    designationperiod_void character varying,
    competentauthority_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedzone integer,
    relatedzone_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE ammngmtrestrictionorregzonepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    thematicid_void character varying,
    name_void character varying,
    specialisedzonetype character varying,
    specialisedzonetype_cl character varying,
    specialisedzonetype_void character varying,
    designationperiod_begin timestamp with time zone,
    designationperiod_end timestamp with time zone,
    designationperiod_void character varying,
    competentauthority_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedzone integer,
    relatedzone_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE ammngmtrestrictionorregzonepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    thematicid_void character varying,
    name_void character varying,
    specialisedzonetype character varying,
    specialisedzonetype_cl character varying,
    specialisedzonetype_void character varying,
    designationperiod_begin timestamp with time zone,
    designationperiod_end timestamp with time zone,
    designationperiod_void character varying,
    competentauthority_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedzone integer,
    relatedzone_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE ammngmtrestrictionorregzonepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    thematicid_void character varying,
    name_void character varying,
    specialisedzonetype character varying,
    specialisedzonetype_cl character varying,
    specialisedzonetype_void character varying,
    designationperiod_begin timestamp with time zone,
    designationperiod_end timestamp with time zone,
    designationperiod_void character varying,
    competentauthority_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedzone integer,
    relatedzone_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ammngmtrestrictionorregzonepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    thematicid_void character varying,
    name_void character varying,
    specialisedzonetype character varying,
    specialisedzonetype_cl character varying,
    specialisedzonetype_void character varying,
    designationperiod_begin timestamp with time zone,
    designationperiod_end timestamp with time zone,
    designationperiod_void character varying,
    competentauthority_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedzone integer,
    relatedzone_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE auadministrativeboundary_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    country character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    legalstatus character varying,
    legalstatus_cl character varying,
    legalstatus_void character varying,
    technicalstatus character varying,
    technicalstatus_cl character varying,
    technicalstatus_void character varying,
    adminunit character varying,
    adminunit_void character varying,
    shape_length double precision
);

CREATE TABLE auadministrativeboundary_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    country character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    legalstatus character varying,
    legalstatus_cl character varying,
    legalstatus_void character varying,
    technicalstatus character varying,
    technicalstatus_cl character varying,
    technicalstatus_void character varying,
    adminunit character varying,
    adminunit_void character varying,
    shape_length double precision
);

CREATE TABLE auadministrativeunit_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    country character varying,
    inspireid character varying,
    name character varying,
    nationalcode character varying,
    nationallevel character varying,
    nationallevel_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    nationallevelname_void character varying,
    residenceofauthority_void character varying,
    boundary character varying,
    boundary_void character varying,
    condominium character varying,
    condominium_void character varying,
    lowerlevelunit integer,
    lowerlevelunit_void character varying,
    upperlevelunit integer,
    upperlevelunit_void character varying,
    administeredby integer,
    administeredby_void character varying,
    coadminister integer,
    coadminister_void character varying,
    nuts character varying,
    id_namedplace_geographicalname character varying,
    id_feature character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE auadministrativeunit_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    country character varying,
    inspireid character varying,
    name character varying,
    nationalcode character varying,
    nationallevel character varying,
    nationallevel_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    localnationallevelname character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    nationallevelname_void character varying,
    residenceofauthority_void character varying,
    boundary character varying,
    boundary_void character varying,
    condominium character varying,
    condominium_void character varying,
    lowerlevelunit integer,
    lowerlevelunit_void character varying,
    upperlevelunit integer,
    upperlevelunit_void character varying,
    administeredby integer,
    administeredby_void character varying,
    coadminister integer,
    coadminister_void character varying,
    nuts character varying,
    id_namedplace_geographicalname character varying,
    id_feature character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE aubaseline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_maritimezone character varying,
    shape_length double precision
);

CREATE TABLE aubaseline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_maritimezone character varying,
    shape_length double precision
);

CREATE TABLE aubaselinesegment_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    segmenttype character varying,
    segmenttype_cl character varying,
    id_baseline character varying,
    shape_length double precision
);

CREATE TABLE aubaselinesegment_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    segmenttype character varying,
    segmenttype_cl character varying,
    id_baseline character varying,
    shape_length double precision
);

CREATE TABLE aucondominium_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    name character varying,
    name_void character varying,
    adminunit character varying,
    adminunit_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE aucondominium_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    name character varying,
    name_void character varying,
    adminunit character varying,
    adminunit_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE aumaritimeboundary_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    legalstatus character varying,
    legalstatus_cl character varying,
    legalstatus_void character varying,
    technicalstatus character varying,
    technicalstatus_cl character varying,
    technicalstatus_void character varying,
    id_maritimezone character varying,
    shape_length double precision
);

CREATE TABLE aumaritimeboundary_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    legalstatus character varying,
    legalstatus_cl character varying,
    legalstatus_void character varying,
    technicalstatus character varying,
    technicalstatus_cl character varying,
    technicalstatus_void character varying,
    id_maritimezone character varying,
    shape_length double precision
);

CREATE TABLE aumaritimezone_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    country character varying,
    inspireid character varying,
    zonetype character varying,
    zonetype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    name character varying,
    name_void character varying,
    boundary_void character varying,
    baseline_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE aumaritimezone_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    country character varying,
    inspireid character varying,
    zonetype character varying,
    zonetype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    name character varying,
    name_void character varying,
    boundary_void character varying,
    baseline_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE auresidenceofauthority_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    name character varying,
    geometry_void character varying,
    id_administrativeunit character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE auresidenceofauthority_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    name character varying,
    id_administrativeunit character varying,
    id_namedplace_geographicalname character varying,
    geometry_void character varying
);

CREATE TABLE brbiogeographicalregion_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    regionclassification character varying,
    regionclassification_cl character varying,
    regionclassificationscheme character varying,
    regionclassificationscheme_cl character varying,
    regionclassificationlevel character varying,
    regionclassificationlevel_cl character varying,
    regionclassificationlevel_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE brbiogeographicalregion_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    regionclassification character varying,
    regionclassification_cl character varying,
    regionclassificationscheme character varying,
    regionclassificationscheme_cl character varying,
    regionclassificationlevel character varying,
    regionclassificationlevel_cl character varying,
    regionclassificationlevel_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuilding_extended_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    conditionofconstruction character varying,
    numberofdwellings integer,
    numberofbuildingunits integer,
    numberoffloorsaboveground integer,
    connectiontoelectricity character varying,
    connectiontogas character varying,
    connectiontosewage character varying,
    connectiontowater character varying,
    energyperformance character varying,
    heightbelowground character varying,
    numberoffloorsbelowground character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    conditionofconstruction_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    buildingnature_void character varying,
    currentuse_void character varying,
    numberofdwellings_void character varying,
    numberofbuildingunits_void character varying,
    numberoffloorsaboveground_void character varying,
    connectiontoelectricity_void character varying,
    connectiontogas_void character varying,
    connectiontosewage_void character varying,
    connectiontowater_void character varying,
    document_void character varying,
    energyperformance_void character varying,
    heatingsource_void character varying,
    heatingsystem_void character varying,
    address_void character varying,
    officialarea_void character varying,
    officialvalue_void character varying,
    heightbelowground_void character varying,
    numberoffloorsbelowground_void character varying,
    floordistribution_void character varying,
    floordescription_void character varying,
    rooftype_void character varying,
    materialoffacade_void character varying,
    materialofroof_void character varying,
    materialofstructure_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuilding_extended_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    conditionofconstruction character varying,
    numberofdwellings integer,
    numberofbuildingunits integer,
    numberoffloorsaboveground integer,
    connectiontoelectricity character varying,
    connectiontogas character varying,
    connectiontosewage character varying,
    connectiontowater character varying,
    energyperformance character varying,
    heightbelowground character varying,
    numberoffloorsbelowground character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    conditionofconstruction_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    buildingnature_void character varying,
    currentuse_void character varying,
    numberofdwellings_void character varying,
    numberofbuildingunits_void character varying,
    numberoffloorsbelowground_void character varying,
    connectiontoelectricity_void character varying,
    connectiontogas_void character varying,
    connectiontosewage_void character varying,
    connectiontowater_void character varying,
    document_void character varying,
    energyperformance_void character varying,
    heatingsource_void character varying,
    heatingsystem_void character varying,
    address_void character varying,
    officialarea_void character varying,
    officialvalue_void character varying,
    heightbelowground_void character varying,
    numberoffloorsaboveground_void character varying,
    floordistribution_void character varying,
    floordescription_void character varying,
    rooftype_void character varying,
    materialoffacade_void character varying,
    materialofroof_void character varying,
    materialofstructure_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuilding_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    conditionofconstruction character varying,
    conditionofconstruction_cl character varying,
    conditionofconstruction_void character varying,
    numberofdwellings integer,
    numberofdwellings_void character varying,
    numberofbuildingunits integer,
    numberofbuildingunits_void character varying,
    numberoffloorsaboveground integer,
    numberoffloorsaboveground_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    buildingnature_void character varying,
    currentuse_void character varying,
    id_namedplace_geographicalname character varying,
    id_feature character varying,
    id_address character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuilding_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    conditionofconstruction character varying,
    conditionofconstruction_cl character varying,
    conditionofconstruction_void character varying,
    numberofdwellings integer,
    numberofdwellings_void character varying,
    numberofbuildingunits integer,
    numberofbuildingunits_void character varying,
    numberoffloorsaboveground integer,
    numberoffloorsaboveground_void character varying,
    dateofconstruction_void character varying,
    dateofrenovation_void character varying,
    dateofdemolition_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    buildingnature_void character varying,
    currentuse_void character varying,
    id_namedplace_geographicalname character varying,
    id_feature character varying,
    id_address character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuildingpart_extended_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    conditionofconstruction character varying,
    numberofdwellings integer,
    numberofbuildingunits integer,
    numberoffloorsaboveground integer,
    connectiontoelectricity character varying,
    connectiontogas character varying,
    connectiontosewage character varying,
    connectiontowater character varying,
    energyperformance character varying,
    heightbelowground character varying,
    numberoffloorsbelowground character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    conditionofconstruction_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    buildingnature_void character varying,
    currentuse_void character varying,
    numberofdwellings_void character varying,
    numberofbuildingunits_void character varying,
    numberoffloorsaboveground_void character varying,
    connectiontoelectricity_void character varying,
    connectiontogas_void character varying,
    connectiontosewage_void character varying,
    connectiontowater_void character varying,
    document_void character varying,
    energyperformance_void character varying,
    heatingsource_void character varying,
    heatingsystem_void character varying,
    address_void character varying,
    officialarea_void character varying,
    officialvalue_void character varying,
    heightbelowground_void character varying,
    numberoffloorsbelowground_void character varying,
    floordistribution_void character varying,
    floordescription_void character varying,
    rooftype_void character varying,
    materialoffacade_void character varying,
    materialofroof_void character varying,
    materialofstructure_void character varying,
    id_building_extended character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuildingpart_extended_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    conditionofconstruction character varying,
    numberofdwellings integer,
    numberofbuildingunits integer,
    numberoffloorsaboveground integer,
    connectiontoelectricity character varying,
    connectiontogas character varying,
    connectiontosewage character varying,
    connectiontowater character varying,
    energyperformance character varying,
    heightbelowground character varying,
    numberoffloorsbelowground_void character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    conditionofconstruction_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    buildingnature_void character varying,
    currentuse_void character varying,
    numberofdwellings_void character varying,
    numberofbuildingunits_void character varying,
    numberoffloorsaboveground_void character varying,
    connectiontoelectricity_void character varying,
    connectiontogas_void character varying,
    connectiontosewage_void character varying,
    connectiontowater_void character varying,
    document_void character varying,
    energyperformance_void character varying,
    heatingsource_void character varying,
    heatingsystem_void character varying,
    address_void character varying,
    officialarea_void character varying,
    officialvalue_void character varying,
    heightbelowground_void character varying,
    numberoffloorsbelowground character varying,
    floordistribution_void character varying,
    floordescription_void character varying,
    rooftype_void character varying,
    materialoffacade_void character varying,
    materialofroof_void character varying,
    materialofstructure_void character varying,
    id_building_extended character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuildingpart_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    conditionofconstruction character varying,
    conditionofconstruction_cl character varying,
    conditionofconstruction_void character varying,
    numberofdwellings integer,
    numberofdwellings_void character varying,
    numberofbuildingunits integer,
    numberofbuildingunits_void character varying,
    numberoffloorsaboveground integer,
    numberoffloorsaboveground_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    buildingnature_void character varying,
    currentuse_void character varying,
    id_building character varying,
    id_namedplace_geographicalname character varying,
    id_address character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuildingpart_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    conditionofconstruction character varying,
    conditionofconstruction_cl character varying,
    conditionofconstruction_void character varying,
    numberofdwellings integer,
    numberofdwellings_void character varying,
    numberofbuildingunits integer,
    numberofbuildingunits_void character varying,
    numberoffloorsaboveground integer,
    numberoffloorsaboveground_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    buildingnature_void character varying,
    currentuse_void character varying,
    id_building character varying,
    id_namedplace_geographicalname character varying,
    id_address character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuildingunit_extended_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    connectiontoelectricity character varying,
    connectiontogas character varying,
    connectiontosewage character varying,
    connectiontowater character varying,
    energyperformance character varying,
    currentuse_void character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    connectiontoelectricity_void character varying,
    connectiontosewage_void character varying,
    connectiontogas_void character varying,
    connectiontowater_void character varying,
    document_void character varying,
    energyperformance_void character varying,
    heatingsource_void character varying,
    heatingsystem_void character varying,
    address_void character varying,
    officialarea_void character varying,
    officialvalue_void character varying,
    id_abstractconstruction character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE bubuildingunit_extended_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    connectiontoelectricity character varying,
    connectiontogas character varying,
    connectiontosewage character varying,
    connectiontowater character varying,
    energyperformance character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    currentuse_void character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    connectiontoelectricity_void character varying,
    connectiontogas_void character varying,
    connectiontosewage_void character varying,
    connectiontowater_void character varying,
    document_void character varying,
    energyperformance_void character varying,
    heatingsource_void character varying,
    heatingsystem_void character varying,
    address_void character varying,
    officialarea_void character varying,
    officialvalue_void character varying,
    id_abstractconstruction character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE buinstallation_extended_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    conditionofconstruction character varying,
    installationnature character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    conditionofconstruction_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    id_abstractconstruction character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE buinstallation_extended_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    conditionofconstruction character varying,
    installationnature character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    conditionofconstruction_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    id_abstractconstruction character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE buotherconstruction_extended_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    conditionofconstruction character varying,
    otherconstructionnature character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    conditionofconstruction_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE buotherconstruction_extended_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    conditionofconstruction character varying,
    otherconstructionnature character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    conditionofconstruction_void character varying,
    dateofconstruction_void character varying,
    dateofdemolition_void character varying,
    dateofrenovation_void character varying,
    elevation_void character varying,
    externalreference_void character varying,
    heightaboveground_void character varying,
    name_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE cpcadastralboundary_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    estimatedaccuracy double precision,
    estimatedaccuracy_uom character varying,
    estimatedaccuracy_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    parcel_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE cpcadastralboundary_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    estimatedaccuracy double precision,
    estimatedaccuracy_uom character varying,
    estimatedaccuracy_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    parcel_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE cpcadastralparcel_referencepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_cadastralparcel character varying
);

CREATE TABLE cpcadastralparcel_referencepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_cadastralparcel character varying
);

CREATE TABLE cpcadastralparcel_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    label character varying,
    nationalcadastralreference character varying,
    areavalue double precision,
    areavalue_uom double precision,
    areavalue_void character varying,
    referencepoint_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    basicpropertyunit_void character varying,
    zoning_void character varying,
    adminunit integer,
    adminunit_void character varying,
    id_cadastralzoning character varying,
    id_basicpropertyunit character varying,
    id_administrativeunit character varying,
    id_building character varying,
    id_address character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE cpcadastralparcel_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    label character varying,
    nationalcadastralreference character varying,
    areavalue double precision,
    areavalue_uom double precision,
    areavalue_void character varying,
    referencepoint_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    basicpropertyunit_void character varying,
    zoning_void character varying,
    adminunit integer,
    adminunit_void character varying,
    id_cadastralzoning character varying,
    id_basicpropertyunit character varying,
    id_administrativeunit character varying,
    id_building character varying,
    id_address character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE cpcadastralzoning_referencepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_cadastralzoning character varying
);

CREATE TABLE cpcadastralzoning_referencepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_cadastralzoning character varying
);

CREATE TABLE cpcadastralzoning_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    label character varying,
    nationalcadastralzoningreference character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    estimatedaccuracy double precision,
    estimatedaccuracy_uom character varying,
    estimatedaccuracy_void character varying,
    level_ character varying,
    level_cl character varying,
    level_void character varying,
    levelname_void character varying,
    name character varying,
    name_void character varying,
    originalmapscaledenominator integer,
    originalmapscaledenominator_void character varying,
    referencepoint_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    upperlevelunit integer,
    upperlevelunit_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE cpcadastralzoning_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    label character varying,
    nationalcadastralzoningreference character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    estimatedaccuracy double precision,
    estimatedaccuracy_uom character varying,
    estimatedaccuracy_void character varying,
    level_ character varying,
    level_cl character varying,
    level_void character varying,
    levelname_void character varying,
    name character varying,
    name_void character varying,
    originalmapscaledenominator integer,
    originalmapscaledenominator_void character varying,
    referencepoint_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    upperlevelunit integer,
    upperlevelunit_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvironmentalmonitoringactivity_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    activitytime character varying,
    activitytime_void character varying,
    activityconditions character varying,
    activityconditions_void character varying,
    boundingbox character varying,
    boundingbox_void character varying,
    setupfor character varying,
    setupfor_void character varying,
    uses character varying,
    uses_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvironmentalmonitoringactivity_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    activitytime character varying,
    activitytime_void character varying,
    activityconditions character varying,
    activityconditions_void character varying,
    boundingbox character varying,
    boundingbox_void character varying,
    setupfor character varying,
    setupfor_void character varying,
    uses character varying,
    uses_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvironmentalmonitoringfacilityline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    measurementregime character varying,
    measurementregime_cl character varying,
    measurementregime_void character varying,
    mobile character varying,
    mobile_void character varying,
    specialisedemftype character varying,
    specialisedemftype_cl character varying,
    specialisedemftype_void character varying,
    operationalactivityperiod character varying,
    operationalactivityperiod_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    belongsto character varying,
    belongsto_void character varying,
    relatedto integer,
    relatedto_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    representativepoint_void character varying,
    resultacquisitionsource_void character varying,
    shape_length double precision
);

CREATE TABLE efenvironmentalmonitoringfacilityline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    measurementregime character varying,
    measurementregime_cl character varying,
    measurementregime_void character varying,
    mobile character varying,
    mobile_void character varying,
    specialisedemftype character varying,
    specialisedemftype_cl character varying,
    specialisedemftype_void character varying,
    operationalactivityperiod character varying,
    operationalactivityperiod_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    belongsto character varying,
    belongsto_void character varying,
    relatedto integer,
    relatedto_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    representativepoint_void character varying,
    resultacquisitionsource_void character varying,
    shape_length double precision
);

CREATE TABLE efenvironmentalmonitoringfacilitypoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    measurementregime character varying,
    measurementregime_cl character varying,
    measurementregime_void character varying,
    mobile character varying,
    mobile_void character varying,
    specialisedemftype character varying,
    specialisedemftype_cl character varying,
    specialisedemftype_void character varying,
    operationalactivityperiod character varying,
    operationalactivityperiod_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    belongsto character varying,
    belongsto_void character varying,
    relatedto integer,
    relatedto_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    representativepoint_void character varying,
    resultacquisitionsource_void character varying
);

CREATE TABLE efenvironmentalmonitoringfacilitypoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    measurementregime character varying,
    measurementregime_cl character varying,
    measurementregime_void character varying,
    mobile character varying,
    mobile_void character varying,
    specialisedemftype character varying,
    specialisedemftype_cl character varying,
    specialisedemftype_void character varying,
    operationalactivityperiod character varying,
    operationalactivityperiod_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    belongsto character varying,
    belongsto_void character varying,
    relatedto integer,
    relatedto_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    representativepoint_void character varying,
    resultacquisitionsource_void character varying
);

CREATE TABLE efenvironmentalmonitoringfacilitypolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    measurementregime character varying,
    measurementregime_cl character varying,
    measurementregime_void character varying,
    mobile character varying,
    mobile_void character varying,
    specialisedemftype character varying,
    specialisedemftype_cl character varying,
    specialisedemftype_void character varying,
    operationalactivityperiod character varying,
    operationalactivityperiod_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader character varying,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    belongsto character varying,
    belongsto_void character varying,
    relatedto integer,
    relatedto_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    representativepoint_void character varying,
    resultacquisitionsource_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvironmentalmonitoringfacilitypolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    measurementregime character varying,
    measurementregime_cl character varying,
    measurementregime_void character varying,
    mobile character varying,
    mobile_void character varying,
    specialisedemftype character varying,
    specialisedemftype_cl character varying,
    specialisedemftype_void character varying,
    operationalactivityperiod character varying,
    operationalactivityperiod_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    belongsto character varying,
    belongsto_void character varying,
    relatedto integer,
    relatedto_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    representativepoint_void character varying,
    resultacquisitionsource_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvironmentalmonitoringnetworkline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    organisationlevel character varying,
    organisationlevel_cl character varying,
    organisationlevel_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    contains character varying,
    contains_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    shape_length double precision
);

CREATE TABLE efenvironmentalmonitoringnetworkline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    organisationlevel character varying,
    organisationlevel_cl character varying,
    organisationlevel_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    contains character varying,
    contains_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    shape_length double precision
);

CREATE TABLE efenvironmentalmonitoringnetworkpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    organisationlevel character varying,
    organisationlevel_cl character varying,
    organisationlevel_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    contains character varying,
    contains_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying
);

CREATE TABLE efenvironmentalmonitoringnetworkpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    organisationlevel character varying,
    organisationlevel_cl character varying,
    organisationlevel_void character varying,
    supersedes character varying,
    supersedes_void integer,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    contains character varying,
    contains_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying
);

CREATE TABLE efenvironmentalmonitoringnetworkpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    organisationlevel character varying,
    organisationlevel_cl character varying,
    organisationlevel_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    contains character varying,
    contains_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvironmentalmonitoringnetworkpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    organisationlevel character varying,
    organisationlevel_cl character varying,
    organisationlevel_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    involvedin character varying,
    involvedin_void character varying,
    hasobservation character varying,
    hasobservation_void character varying,
    contains character varying,
    contains_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    reportedto_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvironmentalmonitoringprogrammeline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    triggers character varying,
    triggers_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    id_feature character varying,
    shape_length double precision
);

CREATE TABLE efenvironmentalmonitoringprogrammeline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    triggers character varying,
    triggers_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    id_feature character varying,
    shape_length double precision
);

CREATE TABLE efenvironmentalmonitoringprogrammepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    triggers character varying,
    triggers_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    id_feature character varying
);

CREATE TABLE efenvironmentalmonitoringprogrammepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    triggers character varying,
    triggers_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    id_feature character varying
);

CREATE TABLE efenvironmentalmonitoringprogrammepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    triggers character varying,
    triggers_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    id_feature character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvironmentalmonitoringprogrammepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    additionaldescription character varying,
    additionaldescription_void character varying,
    supersedes integer,
    supersedes_void character varying,
    supersededby integer,
    supersededby_void character varying,
    narrower integer,
    narrower_void character varying,
    broader integer,
    broader_void character varying,
    observingcapability character varying,
    observingcapability_void character varying,
    triggers character varying,
    triggers_void character varying,
    name_void character varying,
    legalbackground_void character varying,
    responsibleparty_void character varying,
    onlineresource_void character varying,
    purpose_void character varying,
    id_feature character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE efenvmonitoringfacility_representativepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_invironmentalmonitoringfacility character varying
);

CREATE TABLE efenvmonitoringfacility_representativepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_invironmentalmonitoringfacility character varying
);

CREATE TABLE elbreakline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineStringZ,900915),
    propertytype character varying,
    propertytype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    breaklinetype character varying,
    breaklinetype_cl character varying,
    manmadebreak character varying,
    manmadebreak_void character varying,
    id_breakline character varying,
    shape_length double precision
);

CREATE TABLE elbreakline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineStringZ,900914),
    propertytype character varying,
    propertytype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    breaklinetype character varying,
    breaklinetype_cl character varying,
    manmadebreak character varying,
    manmadebreak_void character varying,
    id_breakline character varying,
    shape_length double precision
);

CREATE TABLE elchartdatum_referencepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900915),
    id_chartdatum character varying
);

CREATE TABLE elchartdatum_referencepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900914),
    id_chartdatum character varying
);

CREATE TABLE elcontourline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineStringZ,900915),
    propertytype character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    propertyvalue integer,
    contourlinetype character varying,
    contourlinetype_void character varying,
    downright character varying,
    downright_void character varying,
    id_contourline character varying,
    shape_length double precision
);

CREATE TABLE elcontourline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineStringZ,900914),
    propertytype character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    propertyvalue integer,
    contourlinetype character varying,
    contourlinetype_void character varying,
    downright character varying,
    downright_void character varying,
    id_contourline character varying,
    shape_length double precision
);

CREATE TABLE elisolatedarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    propertytype character varying,
    propertytype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_isolatedarea character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE elisolatedarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    propertytype character varying,
    propertytype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_isolatedarea character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE elspotelevation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900915),
    propertytype character varying,
    propertytype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    propertyvalue integer,
    classification character varying,
    classification_cl character varying,
    classification_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    spotelevationtype character varying,
    spotelevationtype_cl character varying,
    spotelevationtype_void character varying,
    id_spotelevation character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE elspotelevation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900914),
    propertytype character varying,
    propertytype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    propertyvalue integer,
    classification character varying,
    classification_cl character varying,
    classification_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    spotelevationtype character varying,
    spotelevationtype_cl character varying,
    spotelevationtype_void character varying,
    id_spotelevation character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE elvoidarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    propertytype character varying,
    propertytype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_voidarea character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE elvoidarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    propertytype character varying,
    propertytype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_voidarea character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE erfossilfuelresourceline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    dateofdiscovery timestamp with time zone,
    dateofdiscovery_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE erfossilfuelresourceline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    dateofdiscovery timestamp with time zone,
    dateofdiscovery_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE erfossilfuelresourcepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    dateofdiscovery timestamp with time zone,
    dateofdiscovery_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE erfossilfuelresourcepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    dateofdiscovery timestamp with time zone,
    dateofdiscovery_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE erfossilfuelresourcepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    dateofdiscovery timestamp with time zone,
    dateofdiscovery_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE erfossilfuelresourcepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    dateofdiscovery timestamp with time zone,
    dateofdiscovery_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE errenewableandwastepotentialcoverage_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    potentialtype character varying,
    potentialtype_cl character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    assessmentmethod_void character varying,
    name character varying,
    name_void character varying,
    validtime_begin timestamp with time zone,
    validtime_end timestamp with time zone,
    validtime_void character varying,
    verticalextent_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE errenewableandwastepotentialcoverage_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    potentialtype character varying,
    potentialtype_cl character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    assessmentmethod_void character varying,
    name character varying,
    name_void character varying,
    validtime_begin timestamp with time zone,
    validtime_end timestamp with time zone,
    validtime_void character varying,
    verticalextent_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE errenewableandwasteresourceline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    capacity timestamp with time zone,
    capacity_uom character varying,
    capacity_void character varying,
    dateofdetermination timestamp with time zone,
    dateofdetermination_void character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE errenewableandwasteresourceline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    capacity timestamp with time zone,
    capacity_uom character varying,
    capacity_void character varying,
    dateofdetermination timestamp with time zone,
    dateofdetermination_void character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE errenewableandwasteresourcepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    capacity timestamp with time zone,
    capacity_uom character varying,
    capacity_void character varying,
    dateofdetermination timestamp with time zone,
    dateofdetermination_void character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE errenewableandwasteresourcepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    capacity timestamp with time zone,
    capacity_uom character varying,
    capacity_void character varying,
    dateofdetermination timestamp with time zone,
    dateofdetermination_void character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE errenewableandwasteresourcepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    capacity timestamp with time zone,
    capacity_uom character varying,
    capacity_void character varying,
    dateofdetermination timestamp with time zone,
    dateofdetermination_void character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE errenewableandwasteresourcepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    classificationandquantificationframework character varying,
    classificationandquantificationframework_cl character varying,
    verticalextent_void character varying,
    exploitationperiod_void character varying,
    reportingauthority_void character varying,
    resourcename_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    capacity timestamp with time zone,
    capacity_uom character varying,
    capacity_void character varying,
    dateofdetermination timestamp with time zone,
    dateofdetermination_void character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE geactivewellline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    statuscode character varying,
    statuscode_cl character varying,
    statuscode_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying,
    shape_length double precision
);

CREATE TABLE geactivewellline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    statuscode character varying,
    statuscode_cl character varying,
    statuscode_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying,
    shape_length double precision
);

CREATE TABLE geactivewellpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    statuscode character varying,
    statuscode_cl character varying,
    statuscode_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying
);

CREATE TABLE geactivewellpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    statuscode character varying,
    statuscode_cl character varying,
    statuscode_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying
);

CREATE TABLE geactivewellpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    statuscode character varying,
    statuscode_cl character varying,
    statuscode_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE geactivewellpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    statuscode character varying,
    statuscode_cl character varying,
    statuscode_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE geboreholedownholegeometry_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    id_borehole character varying,
    shape_length double precision
);

CREATE TABLE geboreholedownholegeometry_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    id_borehole character varying,
    shape_length double precision
);

CREATE TABLE geboreholelocation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_borehole character varying
);

CREATE TABLE geboreholelocation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_borehole character varying
);

CREATE TABLE gecampaign_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    citation character varying,
    campaigntype character varying,
    campaigntype_cl character varying,
    surveytype character varying,
    surveytype_cl character varying,
    client character varying,
    client_void character varying,
    contractor character varying,
    verticalextent_void character varying,
    distributioninfo_void character varying,
    largerwork_void character varying,
    contractor_void character varying,
    id_geologiccollection character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gecampaign_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    citation character varying,
    campaigntype character varying,
    campaigntype_cl character varying,
    surveytype character varying,
    surveytype_cl character varying,
    client character varying,
    client_void character varying,
    contractor character varying,
    verticalextent_void character varying,
    distributioninfo_void character varying,
    largerwork_void character varying,
    contractor_void character varying,
    id_geologiccollection character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gegeophprofile_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    citation character varying,
    platformtype character varying,
    platformtype_cl character varying,
    profiletype character varying,
    profiletype_cl character varying,
    verticalextent_void character varying,
    distributioninfo_void character varying,
    largerwork_void character varying,
    relatedmodel_void character varying,
    relatednetwork_void character varying,
    id_geologiccollection character varying,
    shape_length double precision
);

CREATE TABLE gegeophprofile_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    citation character varying,
    platformtype character varying,
    platformtype_cl character varying,
    profiletype character varying,
    profiletype_cl character varying,
    verticalextent_void character varying,
    distributioninfo_void character varying,
    largerwork_void character varying,
    relatedmodel_void character varying,
    relatednetwork_void character varying,
    id_geologiccollection character varying,
    shape_length double precision
);

CREATE TABLE gegeophsation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    citation character varying,
    platformtype character varying,
    platformtype_cl character varying,
    stationtype character varying,
    stationtype_cl character varying,
    verticalextent_void character varying,
    distributioninfo_void character varying,
    largerwork_void character varying,
    relatedmodel_void character varying,
    relatednetwork_void character varying,
    stationrank_void character varying,
    id_geologiccollection character varying
);

CREATE TABLE gegeophsation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    citation character varying,
    platformtype character varying,
    platformtype_cl character varying,
    stationtype character varying,
    stationtype_cl character varying,
    verticalextent_void character varying,
    distributioninfo_void character varying,
    largerwork_void character varying,
    relatedmodel_void character varying,
    relatednetwork_void character varying,
    stationrank_void character varying,
    id_geologiccollection character varying
);

CREATE TABLE gegeophswath_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    citation character varying,
    platformtype character varying,
    platformtype_cl character varying,
    swathtype character varying,
    swathtype_cl character varying,
    verticalextent_void character varying,
    distributioninfo_void character varying,
    largerwork_void character varying,
    relatedmodel_void character varying,
    relatednetwork_void character varying,
    id_geologiccollection character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gegeophswath_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    citation character varying,
    platformtype character varying,
    platformtype_cl character varying,
    swathtype character varying,
    swathtype_cl character varying,
    verticalextent_void character varying,
    distributioninfo_void character varying,
    largerwork_void character varying,
    relatedmodel_void character varying,
    relatednetwork_void character varying,
    id_geologiccollection character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gegroundwaterbody_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    conditionofgroundwaterbody character varying,
    conditionofgroundwaterbody_cl character varying,
    mineralization character varying,
    mineralization_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    approximatehorizontalextend_void character varying,
    piezometricstate_void character varying,
    id_wfdgroundwaterbody character varying,
    id_aquifersystem character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gegroundwaterbody_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    conditionofgroundwaterbody character varying,
    conditionofgroundwaterbody_cl character varying,
    mineralization character varying,
    mineralization_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    approximatehorizontalextend_void character varying,
    piezometricstate_void character varying,
    id_wfdgroundwaterbody character varying,
    id_aquifersystem character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gehydrogeologicobjectnaturalline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    naturalobjecttype character varying,
    naturalobjecttype_cl character varying,
    waterpersistence character varying,
    waterpersistence_cl character varying,
    waterpersistence_void character varying,
    approximatequantityofflow_singlequantity double precision,
    approximatequantityofflow_quantityinterval character varying,
    approximatequantityofflow_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying,
    shape_length double precision
);

CREATE TABLE gehydrogeologicobjectnaturalline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    naturalobjecttype character varying,
    naturalobjecttype_cl character varying,
    waterpersistence character varying,
    waterpersistence_cl character varying,
    waterpersistence_void character varying,
    approximatequantityofflow_singlequantity double precision,
    approximatequantityofflow_quantityinterval character varying,
    approximatequantityofflow_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying,
    shape_length double precision
);

CREATE TABLE gehydrogeologicobjectnaturalpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    naturalobjecttype character varying,
    naturalobjecttype_cl character varying,
    waterpersistence character varying,
    waterpersistence_cl character varying,
    waterpersistence_void character varying,
    approximatequantityofflow_singlequantity double precision,
    approximatequantityofflow_quantityinterval character varying,
    approximatequantityofflow_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying
);

CREATE TABLE gehydrogeologicobjectnaturalpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    naturalobjecttype character varying,
    naturalobjecttype_cl character varying,
    waterpersistence character varying,
    waterpersistence_cl character varying,
    waterpersistence_void character varying,
    approximatequantityofflow_singlequantity double precision,
    approximatequantityofflow_quantityinterval character varying,
    approximatequantityofflow_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying
);

CREATE TABLE gehydrogeologicobjectnaturalpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    naturalobjecttype character varying,
    naturalobjecttype_cl character varying,
    waterpersistence character varying,
    waterpersistence_void character varying,
    waterpersistence_cl character varying,
    approximatequantityofflow_singlequantity double precision,
    approximatequantityofflow_quantityinterval character varying,
    approximatequantityofflow_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gehydrogeologicobjectnaturalpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    description character varying,
    description_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    naturalobjecttype character varying,
    naturalobjecttype_cl character varying,
    waterpersistence character varying,
    waterpersistence_void character varying,
    waterpersistence_cl character varying,
    approximatequantityofflow_singlequantity double precision,
    approximatequantityofflow_quantityinterval character varying,
    approximatequantityofflow_void character varying,
    id_groundwaterbody character varying,
    id_aquifer character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gemappedfeatureline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    mappingframe character varying,
    mappingframe_cl character varying,
    id_geologicfeature character varying,
    id_geologiccollection character varying,
    shape_length double precision
);

CREATE TABLE gemappedfeatureline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    mappingframe character varying,
    mappingframe_cl character varying,
    id_geologicfeature character varying,
    id_geologiccollection character varying,
    shape_length double precision
);

CREATE TABLE gemappedfeaturepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    mappingframe character varying,
    mappingframe_cl character varying,
    id_geologicfeature character varying,
    id_geologiccollection character varying
);

CREATE TABLE gemappedfeaturepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    mappingframe character varying,
    mappingframe_cl character varying,
    id_geologicfeature character varying,
    id_geologiccollection character varying
);

CREATE TABLE gemappedfeaturepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    mappingframe character varying,
    mappingframe_cl character varying,
    id_geologicfeature character varying,
    id_geologiccollection character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gemappedfeaturepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    mappingframe character varying,
    mappingframe_cl character varying,
    id_geologicfeature character varying,
    id_geologiccollection character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gnnamedplaceline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    name character varying,
    type_void character varying,
    localtype_void character varying,
    relatedspatialobject_void character varying,
    leastdetailedviewingscale integer,
    leastdetailedviewingscale_void character varying,
    mostdetailedviewingscale integer,
    mostdetailedviewingscale_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision
);

CREATE TABLE gnnamedplaceline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    name character varying,
    type_void character varying,
    localtype_void character varying,
    relatedspatialobject_void character varying,
    leastdetailedviewingscale integer,
    leastdetailedviewingscale_void character varying,
    mostdetailedviewingscale integer,
    mostdetailedviewingscale_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision
);

CREATE TABLE gnnamedplacemultipoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPoint,900915),
    inspireid character varying,
    name character varying,
    type_void character varying,
    localtype_void character varying,
    relatedspatialobject_void character varying,
    leastdetailedviewingscale integer,
    leastdetailedviewingscale_void character varying,
    mostdetailedviewingscale integer,
    mostdetailedviewingscale_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying
);

CREATE TABLE gnnamedplacemultipoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPoint,900914),
    inspireid character varying,
    name character varying,
    type_void character varying,
    localtype_void character varying,
    relatedspatialobject_void character varying,
    leastdetailedviewingscale integer,
    leastdetailedviewingscale_void character varying,
    mostdetailedviewingscale integer,
    mostdetailedviewingscale_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying
);

CREATE TABLE gnnamedplacepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    name character varying,
    type_void character varying,
    localtype_void character varying,
    relatedspatialobject_void character varying,
    leastdetailedviewingscale integer,
    leastdetailedviewingscale_void character varying,
    mostdetailedviewingscale integer,
    mostdetailedviewingscale_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying
);

CREATE TABLE gnnamedplacepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    name character varying,
    type_void character varying,
    localtype_void character varying,
    relatedspatialobject_void character varying,
    leastdetailedviewingscale integer,
    leastdetailedviewingscale_void character varying,
    mostdetailedviewingscale integer,
    mostdetailedviewingscale_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying
);

CREATE TABLE gnnamedplacepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    name character varying,
    type_void character varying,
    localtype_void character varying,
    relatedspatialobject_void character varying,
    leastdetailedviewingscale integer,
    leastdetailedviewingscale_void character varying,
    mostdetailedviewingscale integer,
    mostdetailedviewingscale_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE gnnamedplacepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    name character varying,
    type_void character varying,
    localtype_void character varying,
    relatedspatialobject_void character varying,
    leastdetailedviewingscale integer,
    leastdetailedviewingscale_void character varying,
    mostdetailedviewingscale integer,
    mostdetailedviewingscale_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hbhabitatline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    habitatspecies_void character varying,
    habitatvegetation_void character varying,
    id_protectedsite character varying,
    shape_length double precision
);

CREATE TABLE hbhabitatline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    habitatspecies_void character varying,
    habitatvegetation_void character varying,
    id_protectedsite character varying,
    shape_length double precision
);

CREATE TABLE hbhabitatpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    habitatspecies_void character varying,
    habitatvegetation_void character varying,
    id_protectedsite character varying
);

CREATE TABLE hbhabitatpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    habitatspecies_void character varying,
    habitatvegetation_void character varying,
    id_protectedsite character varying
);

CREATE TABLE hbhabitatpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    habitatspecies_void character varying,
    habitatvegetation_void character varying,
    id_protectedsite character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hbhabitatpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    habitatspecies_void character varying,
    habitatvegetation_void character varying,
    id_protectedsite character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hycrossingline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    id_namedplace_geographicalname character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    shape_length double precision
);

CREATE TABLE hycrossingline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    id_namedplace_geographicalname character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    shape_length double precision
);

CREATE TABLE hycrossingpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hycrossingpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hycrossingpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hycrossingpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hydamorweirline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hydamorweirline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hydamorweirpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hydamorweirpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hydamorweirpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hydamorweirpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hydrainagebasin_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    area double precision,
    area_void character varying,
    basinorder_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    outlet character varying,
    outlet_void character varying,
    containsbasin integer,
    containsbasin_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hydrainagebasin_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    area double precision,
    area_void character varying,
    basinorder_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    outlet character varying,
    outlet_void character varying,
    containsbasin integer,
    containsbasin_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyembankmentline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyembankmentline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyembankmentpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyembankmentpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyembankmentpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyembankmentpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyfalls_lineutm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    height double precision,
    height_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyfallsline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    height double precision,
    height_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyfallspoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    height double precision,
    height_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyfallspoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    height double precision,
    height_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyfallspolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    height double precision,
    height_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyfallspolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    height double precision,
    height_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyfordline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyfordline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyfordpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyfordpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyfordpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyfordpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyhydronode_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    hydronodecategory character varying,
    hydronodecategory_cl character varying,
    hydronodecategory_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyhydronode_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    hydronodecategory character varying,
    hydronodecategory_cl character varying,
    hydronodecategory_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hylandwaterboundary_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    waterlevelcategory character varying,
    waterlevelcategory_cl character varying,
    waterlevelcategory_void character varying,
    shape_length double precision
);

CREATE TABLE hylandwaterboundary_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    waterlevelcategory character varying,
    waterlevelcategory_cl character varying,
    waterlevelcategory_void character varying,
    shape_length double precision
);

CREATE TABLE hyrapidsline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyrapidsline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyrapidspoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyrapidspoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyrapidspolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyrapidspolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyriverbasin_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    area double precision,
    area_void character varying,
    basinorder_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    outlet character varying,
    outlet_void character varying,
    containsbasin integer,
    containsbasin_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyriverbasin_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    area double precision,
    area_void character varying,
    basinorder_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    outlet character varying,
    outlet_void character varying,
    containsbasin integer,
    containsbasin_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyshore_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    composition character varying,
    composition_cl character varying,
    composition_void character varying,
    delineationknown character varying,
    delineationknown_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    id_watercourse character varying,
    id_standingwater character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyshore_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    composition character varying,
    composition_cl character varying,
    composition_void character varying,
    delineationknown character varying,
    delineationknown_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    id_watercourse character varying,
    id_standingwater character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyshorelineconstructionline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyshorelineconstructionline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hyshorelineconstructionpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyshorelineconstructionpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hyshorelineconstructionpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hyshorelineconstructionpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hysluiceline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hysluiceline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hysluicepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hysluicepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hysluicepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hysluicepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geometry_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hystandingwaterpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    origin character varying,
    origin_cl character varying,
    bank character varying,
    origin_void character varying,
    persistence character varying,
    persistence_cl character varying,
    persistence_void character varying,
    tidal character varying,
    tidal_void character varying,
    elevation double precision,
    elevation_void character varying,
    meandepth double precision,
    meandepth_void character varying,
    surfacearea double precision,
    surfacearea_void character varying,
    drainsbasin character varying,
    drainsbasin_void character varying,
    bank_void character varying,
    neighbour integer,
    neighbour_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hystandingwaterpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    persistence character varying,
    persistence_cl character varying,
    persistence_void character varying,
    tidal character varying,
    tidal_void character varying,
    elevation double precision,
    elevation_void character varying,
    meandepth double precision,
    meandepth_void character varying,
    surfacearea double precision,
    surfacearea_void character varying,
    drainsbasin character varying,
    drainsbasin_void character varying,
    bank_void character varying,
    neighbour integer,
    neighbour_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE hystandingwaterpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    persistence character varying,
    persistence_cl character varying,
    persistence_void character varying,
    tidal character varying,
    tidal_void character varying,
    elevation double precision,
    elevation_void character varying,
    meandepth double precision,
    meandepth_void character varying,
    surfacearea double precision,
    surfacearea_void character varying,
    drainsbasin character varying,
    drainsbasin_void character varying,
    bank_void character varying,
    neighbour integer,
    neighbour_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hystandingwaterpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    persistence character varying,
    persistence_cl character varying,
    persistence_void character varying,
    tidal character varying,
    tidal_void character varying,
    elevation double precision,
    elevation_void character varying,
    meandepth double precision,
    meandepth_void character varying,
    surfacearea double precision,
    surfacearea_void character varying,
    drainsbasin character varying,
    drainsbasin_void character varying,
    bank_void character varying,
    neighbour integer,
    neighbour_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hywatercourseline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    persistence character varying,
    persistence_cl character varying,
    persistence_void character varying,
    tidal character varying,
    tidal_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    delineationknown character varying,
    delineationknown_void character varying,
    length double precision,
    length_void character varying,
    level_ character varying,
    level_cl character varying,
    level_void character varying,
    streamorder_void character varying,
    width_void character varying,
    drainsbasin character varying,
    drainsbasin_void character varying,
    bank_void character varying,
    neighbour integer,
    neighbour_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hywatercourseline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    persistence character varying,
    persistence_cl character varying,
    persistence_void character varying,
    tidal character varying,
    tidal_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    delineationknown character varying,
    delineationknown_void character varying,
    length double precision,
    length_void character varying,
    level_ character varying,
    level_cl character varying,
    level_void character varying,
    streamorder_void character varying,
    width_void character varying,
    drainsbasin character varying,
    drainsbasin_void character varying,
    bank_void character varying,
    neighbour integer,
    neighbour_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hywatercourselink_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    fictitious character varying,
    flowdirection character varying,
    flowdirection_cl character varying,
    flowdirection_void character varying,
    length double precision,
    length_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    id_watercourseseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE hywatercourselink_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    fictitious character varying,
    flowdirection character varying,
    flowdirection_cl character varying,
    flowdirection_void character varying,
    length double precision,
    length_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    id_watercourseseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE hywatercourselinksequence_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hywatercourselinksequence_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE hywatercoursepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    persistence character varying,
    persistence_cl character varying,
    persistence_void character varying,
    tidal character varying,
    tidal_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    delineationknown character varying,
    delineationknown_void character varying,
    length double precision,
    length_void character varying,
    level_ character varying,
    level_cl character varying,
    level_void character varying,
    streamorder_void character varying,
    width_void character varying,
    drainsbasin character varying,
    drainsbasin_void character varying,
    bank_void character varying,
    neighbour integer,
    neighbour_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hywatercoursepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    levelofdetail integer,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    origin character varying,
    origin_cl character varying,
    origin_void character varying,
    persistence character varying,
    persistence_cl character varying,
    persistence_void character varying,
    tidal character varying,
    tidal_void character varying,
    condition character varying,
    condition_cl character varying,
    condition_void character varying,
    delineationknown character varying,
    delineationknown_void character varying,
    length double precision,
    length_void character varying,
    level_ character varying,
    level_cl character varying,
    level_void character varying,
    streamorder_void character varying,
    width_void character varying,
    drainsbasin character varying,
    drainsbasin_void character varying,
    bank_void character varying,
    neighbour integer,
    neighbour_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hywetland_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    tidal character varying,
    tidal_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE hywetland_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    localtype character varying,
    localtype_local character varying,
    localtype_void character varying,
    tidal character varying,
    tidal_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE lclandcoverunitpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_landcoverdataset character varying
);

CREATE TABLE lclandcoverunitpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_landcoverdataset character varying
);

CREATE TABLE lclandcoverunitpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_landcoverdataset character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE lclandcoverunitpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_landcoverdataset character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luexistinglandusedataset_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    name character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luexistinglandusedataset_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    name character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luexistinglanduseobject_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    observationdate timestamp with time zone,
    observationdate_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    hilucspresence_void character varying,
    specificlanduse_void character varying,
    specificpresence_void character varying,
    id_existinglandusedataset character varying,
    id_hilucspresence character varying,
    id_specificpresence character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luexistinglanduseobject_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    observationdate timestamp with time zone,
    observationdate_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    hilucspresence_void character varying,
    specificlanduse_void character varying,
    specificpresence_void character varying,
    id_existinglandusedataset character varying,
    id_hilucspresence character varying,
    id_specificpresence character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luexistinglandusesample_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    observationdate timestamp with time zone,
    observationdate_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    hilucspresence_void character varying,
    specificlanduse_void character varying,
    specificpresence_void character varying,
    id_sampleexistinglandusedataset character varying,
    id_hilucspresence character varying,
    id_specificpresence character varying
);

CREATE TABLE luexistinglandusesample_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    observationdate timestamp with time zone,
    observationdate_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    hilucspresence_void character varying,
    specificlanduse_void character varying,
    specificpresence_void character varying,
    id_sampleexistinglandusedataset character varying,
    id_hilucspresence character varying,
    id_specificpresence character varying
);

CREATE TABLE lusampledexistinglandusedataset_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    name character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE lusampledexistinglandusedataset_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    name character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luspatialplan_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    levelofspatialplan character varying,
    levelofspatialplan_cl character varying,
    officialtitle character varying,
    
    
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    plantypename character varying,
    backgroundmap_void character varying,
    ordinance_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luspatialplan_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    levelofspatialplan character varying,
    levelofspatialplan_cl character varying,
    officialtitle character varying,
    
    
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    plantypename character varying,
    backgroundmap_void character varying,
    ordinance_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE lusupplementaryregulationline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    regulationnature character varying,
    regulationnature_cl character varying,
    inheritedfromotherplans character varying,
    inheritedfromotherplans_void character varying,
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    specificregulationnature character varying,
    specificregulationnature_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    backgroundmap_void character varying,
    dimensioningindication_void character varying,
    name_void character varying,
    specificsupplementaryregulation_void character varying,
    id_spatialplan character varying,
    shape_length double precision
);

CREATE TABLE lusupplementaryregulationline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    regulationnature character varying,
    regulationnature_cl character varying,
    inheritedfromotherplans character varying,
    inheritedfromotherplans_void character varying,
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    specificregulationnature character varying,
    specificregulationnature_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    backgroundmap_void character varying,
    dimensioningindication_void character varying,
    name_void character varying,
    specificsupplementaryregulation_void character varying,
    id_spatialplan character varying,
    shape_length double precision
);

CREATE TABLE lusupplementaryregulationpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    regulationnature character varying,
    regulationnature_cl character varying,
    inheritedfromotherplans character varying,
    inheritedfromotherplans_void character varying,
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    specificregulationnature character varying,
    specificregulationnature_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    backgroundmap_void character varying,
    dimensioningindication_void character varying,
    name_void character varying,
    specificsupplementaryregulation_void character varying,
    id_spatialplan character varying
);

CREATE TABLE lusupplementaryregulationpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    regulationnature character varying,
    regulationnature_cl character varying,
    inheritedfromotherplans character varying,
    inheritedfromotherplans_void character varying,
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    specificregulationnature character varying,
    specificregulationnature_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    backgroundmap_void character varying,
    dimensioningindication_void character varying,
    name_void character varying,
    specificsupplementaryregulation_void character varying,
    id_spatialplan character varying
);

CREATE TABLE lusupplementaryregulationpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    regulationnature character varying,
    regulationnature_cl character varying,
    inheritedfromotherplans character varying,
    inheritedfromotherplans_void character varying,
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    specificregulationnature character varying,
    specificregulationnature_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    backgroundmap_void character varying,
    dimensioningindication_void character varying,
    name_void character varying,
    specificsupplementaryregulation_void character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    id_spatialplan character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE lusupplementaryregulationpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    regulationnature character varying,
    regulationnature_cl character varying,
    inheritedfromotherplans character varying,
    inheritedfromotherplans_void character varying,
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    specificregulationnature character varying,
    specificregulationnature_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    backgroundmap_void character varying,
    dimensioningindication_void character varying,
    name_void character varying,
    specificsupplementaryregulation_void character varying,
    id_spatialplan character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luzoningelement_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    regulationnature character varying,
    regulationnature_cl character varying,
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    backgroundmap_void character varying,
    dimensioningindication_void character varying,
    hilucspresence_void character varying,
    specificlanduse_void character varying,
    specificpresence_void character varying,
    id_spatialplan character varying,
    id_hilucspresence character varying,
    id_specificpresence character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE luzoningelement_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    regulationnature character varying,
    regulationnature_cl character varying,
    processstepgeneral character varying,
    processstepgeneral_cl character varying,
    processstepgeneral_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    specificpresence_void character varying,
    specificlanduse_void character varying,
    hilucspresence_void character varying,
    dimensioningindication_void character varying,
    backgroundmap_void character varying,
    id_spatialplan character varying,
    id_hilucspresence character varying,
    id_specificpresence character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE mrminingfeatureoccurrenceline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    id_miningfeature character varying,
    shape_length double precision
);

CREATE TABLE mrminingfeatureoccurrenceline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    id_miningfeature character varying,
    shape_length double precision
);

CREATE TABLE mrminingfeatureoccurrencepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_miningfeature character varying
);

CREATE TABLE mrminingfeatureoccurrencepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_miningfeature character varying
);

CREATE TABLE mrminingfeatureoccurrencepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_miningfeature character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE mrminingfeatureoccurrencepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_miningfeature character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE nzexposedelementsline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    sourceofspatialrepresentation_void character varying,
    assessmentofvulnerability_void character varying,
    id_abstractriskzone character varying,
    shape_length double precision
);

CREATE TABLE nzexposedelementsline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    sourceofspatialrepresentation_void character varying,
    assessmentofvulnerability_void character varying,
    id_abstractriskzone character varying,
    shape_length double precision
);

CREATE TABLE nzexposedelementspoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    sourceofspatialrepresentation_void character varying,
    assessmentofvulnerability_void character varying,
    id_abstractriskzone character varying
);

CREATE TABLE nzexposedelementspoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    sourceofspatialrepresentation_void character varying,
    assessmentofvulnerability_void character varying,
    id_abstractriskzone character varying
);

CREATE TABLE nzexposedelementspolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    sourceofspatialrepresentation_void character varying,
    assessmentofvulnerability_void character varying,
    id_abstractriskzone character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE nzexposedelementspolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    sourceofspatialrepresentation_void character varying,
    assessmentofvulnerability_void character varying,
    id_abstractriskzone character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE nzhazardarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    determinationmethod character varying,
    determinationmethod_cl character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    inspireid character varying,
    validityperiod_begin timestamp with time zone,
    validityperiod_end timestamp with time zone,
    validityperiod_void character varying,
    likelihoodofoccurrence_void character varying,
    magnitudeorintensity_void character varying,
    source_void character varying,
    id_abstractobservedevent character varying,
    id_abstractriskzone character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE nzhazardarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    determinationmethod character varying,
    determinationmethod_cl character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    inspireid character varying,
    validityperiod_begin timestamp with time zone,
    validityperiod_end timestamp with time zone,
    validityperiod_void character varying,
    likelihoodofoccurrence_void character varying,
    magnitudeorintensity_void character varying,
    source_void character varying,
    id_abstractobservedevent character varying,
    id_abstractriskzone character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE nzobservedeventline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    nameofevent character varying,
    nameofevent_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    magnitudeorintensity_void character varying,
    ismonitoredby_void character varying,
    id_abstracthazardarea character varying,
    shape_length double precision
);

CREATE TABLE nzobservedeventline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    nameofevent character varying,
    nameofevent_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    magnitudeorintensity_void character varying,
    ismonitoredby_void character varying,
    id_abstracthazardarea character varying,
    shape_length double precision
);

CREATE TABLE nzobservedeventpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    nameofevent character varying,
    nameofevent_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    magnitudeorintensity_void character varying,
    ismonitoredby_void character varying,
    id_abstracthazardarea character varying
);

CREATE TABLE nzobservedeventpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    nameofevent character varying,
    nameofevent_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    magnitudeorintensity_void character varying,
    ismonitoredby_void character varying,
    id_abstracthazardarea character varying
);

CREATE TABLE nzobservedeventpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    nameofevent character varying,
    nameofevent_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    magnitudeorintensity_void character varying,
    ismonitoredby_void character varying,
    id_abstracthazardarea character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE nzobservedeventpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    nameofevent character varying,
    nameofevent_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    magnitudeorintensity_void character varying,
    ismonitoredby_void character varying,
    id_abstracthazardarea character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE nzriskzone_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validityperiod_begin timestamp with time zone,
    validityperiod_end timestamp with time zone,
    validityperiod_void character varying,
    exposedelement_void character varying,
    source_void character varying,
    levelofrisk_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE nzriskzone_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validityperiod_begin timestamp with time zone,
    validityperiod_end timestamp with time zone,
    validityperiod_void character varying,
    exposedelement_void character varying,
    source_void character varying,
    levelofrisk_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ofgridobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_observation character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ofgridobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_observation character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ofgridseriesobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_observation character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ofgridseriesobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_observation character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ofmultipointobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPoint,900915),
    id_observation character varying
);

CREATE TABLE ofmultipointobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPoint,900914),
    id_observation character varying
);

CREATE TABLE ofpointobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_observation character varying
);

CREATE TABLE ofpointobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_observation character varying
);

CREATE TABLE ofpointtimeseriesobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_observation character varying
);

CREATE TABLE ofpointtimeseriesobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_observation character varying
);

CREATE TABLE ofsamplingcurve_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    id_samplingcurve character varying,
    shape_length double precision
);

CREATE TABLE ofsamplingcurve_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    id_samplingcurve character varying,
    shape_length double precision
);

CREATE TABLE ofsamplingpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_samplingpoint character varying
);

CREATE TABLE ofsamplingpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_samplingpoint character varying
);

CREATE TABLE ofsamplingsolid_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    id_samplingsolid character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ofsamplingsolid_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    id_samplingsolid character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ofsamplingsurface_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_samplingsurface character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ofsamplingsurface_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_samplingsurface character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE oftrajectoryobservation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    id_observation character varying,
    shape_length double precision
);

CREATE TABLE oftrajectoryobservation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    id_observation character varying,
    shape_length double precision
);

CREATE TABLE oiaggregatedmosaicelement_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    phenomenontime_begin timestamp with time zone,
    phenomenontime_end timestamp with time zone,
    id_orthoimagecoverage character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE oiaggregatedmosaicelement_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    phenomenontime_begin timestamp with time zone,
    phenomenontime_end timestamp with time zone,
    id_orthoimagecoverage character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE oisinglemosaicelement_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    phenomenontime_begin timestamp with time zone,
    phenomenontime_end timestamp with time zone,
    imagesourcereference character varying,
    imagesourcereference_void character varying,
    id_orthoimagecoverage character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE oisinglemosaicelement_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    phenomenontime_begin timestamp with time zone,
    phenomenontime_end timestamp with time zone,
    imagesourcereference character varying,
    imagesourcereference_void character varying,
    id_orthoimagecoverage character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pdstatisticaldistribution_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    universe character varying,
    domain_ character varying,
    measure character varying,
    measure_cl character varying,
    measurementmethod character varying,
    measurementmethod_cl character varying,
    measurementunit character varying,
    notcountedproportion double precision,
    periodofmeasurement_begin timestamp with time zone,
    periodofmeasurement_end timestamp with time zone,
    periodofreference_begin timestamp with time zone,
    periodofreference_end timestamp with time zone,
    periodofvalidity_begin timestamp with time zone,
    periodofvalidity_end timestamp with time zone,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    generalstatus character varying,
    generalstatus_cl character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pdstatisticaldistribution_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    universe character varying,
    domain_ character varying,
    measure character varying,
    measure_cl character varying,
    measurementmethod character varying,
    measurementmethod_cl character varying,
    measurementunit character varying,
    notcountedproportion double precision,
    periodofmeasurement_begin timestamp with time zone,
    periodofmeasurement_end timestamp with time zone,
    periodofreference_begin timestamp with time zone,
    periodofreference_end timestamp with time zone,
    periodofvalidity_begin timestamp with time zone,
    periodofvalidity_end timestamp with time zone,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    generalstatus character varying,
    generalstatus_cl character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pfproductionbuildingline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineStringZ,900915),
    building character varying,
    groupedfacility character varying,
    typeofbuilding_void character varying,
    status_void character varying,
    geometry_void character varying,
    building_void character varying,
    id_productionbuilding character varying,
    shape_length double precision
);

CREATE TABLE pfproductionbuildingline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineStringZ,900914),
    building character varying,
    groupedfacility character varying,
    typeofbuilding_void character varying,
    status_void character varying,
    geometry_void character varying,
    building_void character varying,
    id_productionbuilding character varying,
    shape_length double precision
);

CREATE TABLE pfproductionbuildingpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900915),
    building character varying,
    groupedfacility character varying,
    typeofbuilding_void character varying,
    status_void character varying,
    geometry_void character varying,
    building_void character varying,
    id_productionbuilding character varying
);

CREATE TABLE pfproductionbuildingpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900914),
    building character varying,
    groupedfacility character varying,
    typeofbuilding_void character varying,
    status_void character varying,
    geometry_void character varying,
    building_void character varying,
    id_productionbuilding character varying
);

CREATE TABLE pfproductionbuildingpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    building character varying,
    groupedfacility character varying,
    typeofbuilding_void character varying,
    status_void character varying,
    geometry_void character varying,
    building_void character varying,
    id_productionbuilding character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pfproductionbuildingpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    building character varying,
    groupedfacility character varying,
    typeofbuilding_void character varying,
    status_void character varying,
    geometry_void character varying,
    building_void character varying,
    id_productionbuilding character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pfproductionfacility_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    riverbasindistrict character varying,
    riverbasindistrict_cl character varying,
    hostingsite character varying,
    groupedplot character varying,
    groupedinstallation character varying,
    groupedbuilding character varying,
    surfacegeometry_void character varying,
    status_void character varying,
    hostingsite_void character varying,
    groupedplot_void character varying,
    groupedinstallation_void character varying,
    groupedbuilding_void character varying,
    id_productionsite character varying,
    id_productionfacility character varying,
    id_administrativeunit character varying,
    id_cadastralparcel character varying,
    id_existinglanduseobject character varying,
    id_managementrestrictionorregulationzone character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pfproductionfacility_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    riverbasindistrict character varying,
    riverbasindistrict_cl character varying,
    hostingsite character varying,
    groupedplot character varying,
    groupedinstallation character varying,
    groupedbuilding character varying,
    surfacegeometry_void character varying,
    status_void character varying,
    hostingsite_void character varying,
    groupedplot_void character varying,
    groupedinstallation_void character varying,
    groupedbuilding_void character varying,
    id_productionsite character varying,
    id_productionfacility character varying,
    id_administrativeunit character varying,
    id_cadastralparcel character varying,
    id_existinglanduseobject character varying,
    id_managementrestrictionorregulationzone character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pfproductioninstallation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900915),
    inspireid character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    groupingfacility character varying,
    groupedinstallationpart character varying,
    groupedinstallationpart_void character varying,
    surfacegeometry_void character varying,
    name_void character varying,
    description_void character varying,
    status_void character varying,
    id_productionfacility character varying
);

CREATE TABLE pfproductioninstallation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900914),
    inspireid character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    groupingfacility character varying,
    groupedinstallationpart character varying,
    groupedinstallationpart_void character varying,
    surfacegeometry_void character varying,
    name_void character varying,
    description_void character varying,
    status_void character varying,
    id_productionfacility character varying
);

CREATE TABLE pfproductioninstallationpart_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900915),
    inspireid character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    technique character varying,
    technique_cl character varying,
    technique_void character varying,
    groupinginstallation character varying,
    surfacegeometry_void character varying,
    name_void character varying,
    description_void character varying,
    status_void character varying,
    id_productioninstallation character varying
);

CREATE TABLE pfproductioninstallationpart_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(PointZ,900914),
    inspireid character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    technique character varying,
    technique_cl character varying,
    technique_void character varying,
    groupinginstallation character varying,
    surfacegeometry_void character varying,
    name_void character varying,
    description_void character varying,
    status_void character varying,
    id_productioninstallation character varying
);

CREATE TABLE pfproductionplot_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    groupingfacility character varying,
    status_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pfproductionplot_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    groupingfacility character varying,
    status_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pfproductionsite_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900915),
    inspireid character varying,
    hostedfacility character varying,
    siteplan_void character varying,
    name_void character varying,
    description_void character varying,
    status_void character varying,
    hostedfacility_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE pfproductionsite_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygonZ,900914),
    inspireid character varying,
    hostedfacility character varying,
    siteplan_void character varying,
    name_void character varying,
    description_void character varying,
    status_void character varying,
    hostedfacility_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE psprotectedsiteline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    legalfoundationdate timestamp with time zone,
    legalfoundationdate_void character varying,
    legalfoundationdocument_void character varying,
    sitedesignation_void character varying,
    sitename_void character varying,
    siteprotectionclassification_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE psprotectedsiteline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    legalfoundationdate timestamp with time zone,
    legalfoundationdate_void character varying,
    legalfoundationdocument_void character varying,
    sitedesignation_void character varying,
    sitename_void character varying,
    siteprotectionclassification_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE psprotectedsitemultipoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPoint,900915),
    inspireid character varying,
    legalfoundationdate timestamp with time zone,
    legalfoundationdate_void character varying,
    legalfoundationdocument_void character varying,
    sitedesignation_void character varying,
    sitename_void character varying,
    siteprotectionclassification_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE psprotectedsitemultipoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPoint,900914),
    inspireid character varying,
    legalfoundationdate timestamp with time zone,
    legalfoundationdate_void character varying,
    legalfoundationdocument_void character varying,
    sitedesignation_void character varying,
    sitename_void character varying,
    siteprotectionclassification_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE psprotectedsitepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    legalfoundationdate timestamp with time zone,
    legalfoundationdate_void character varying,
    legalfoundationdocument_void character varying,
    sitedesignation_void character varying,
    sitename_void character varying,
    siteprotectionclassification_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE psprotectedsitepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    legalfoundationdate timestamp with time zone,
    legalfoundationdate_void character varying,
    legalfoundationdocument_void character varying,
    sitedesignation_void character varying,
    sitename_void character varying,
    siteprotectionclassification_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE psprotectedsitepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    legalfoundationdate timestamp with time zone,
    legalfoundationdate_void character varying,
    legalfoundationdocument_void character varying,
    sitedesignation_void character varying,
    sitename_void character varying,
    siteprotectionclassification_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE psprotectedsitepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    legalfoundationdate timestamp with time zone,
    legalfoundationdate_void character varying,
    legalfoundationdocument_void character varying,
    sitedesignation_void character varying,
    sitename_void character varying,
    siteprotectionclassification_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sdspeciesdistributiondataset_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    name character varying,
    name_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sdspeciesdistributiondataset_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    name character varying,
    name_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sdspeciesdistributionunitline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    distributioninfo_void character varying,
    id_speciesdistributiondataset character varying,
    shape_length double precision
);

CREATE TABLE sdspeciesdistributionunitline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    distributioninfo_void character varying,
    id_speciesdistributiondataset character varying,
    shape_length double precision
);

CREATE TABLE sdspeciesdistributionunitpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    distributioninfo_void character varying,
    id_speciesdistributiondataset character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sdspeciesdistributionunitpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    distributioninfo_void character varying,
    id_speciesdistributiondataset character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sosoilbody_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    soilbodylabel character varying,
    soilbodylabel_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_soilderivedobject character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sosoilbody_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    soilbodylabel character varying,
    soilbodylabel_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_soilderivedobject character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sosoilderivedobjectline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    shape_length double precision
);

CREATE TABLE sosoilderivedobjectline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    shape_length double precision
);

CREATE TABLE sosoilderivedobjectpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying
);

osoilderivedobjectpoint_utm25n_ogc_fid_seq OWNER TO postgres;

CREATE TABLE sosoilderivedobjectpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying
);

CREATE TABLE sosoilderivedobjectpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sosoilderivedobjectpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sosoilplotlocation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    soilplotlocation character varying,
    soilplottype character varying,
    soilplottype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_soilsite character varying,
    id_observedsoilprofile character varying
);

CREATE TABLE sosoilplotlocation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    soilplotlocation character varying,
    soilplottype character varying,
    soilplottype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_soilsite character varying,
    id_observedsoilprofile character varying
);

CREATE TABLE sosoilsiteline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    soilinvestigationpurpose character varying,
    soilinvestigationpurpose_cl character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision
);

CREATE TABLE sosoilsiteline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    soilinvestigationpurpose character varying,
    soilinvestigationpurpose_cl character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision
);

CREATE TABLE sosoilsitepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    soilinvestigationpurpose character varying,
    soilinvestigationpurpose_cl character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying
);

CREATE TABLE sosoilsitepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    soilinvestigationpurpose character varying,
    soilinvestigationpurpose_cl character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying
);

CREATE TABLE sosoilsitepolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    soilinvestigationpurpose character varying,
    soilinvestigationpurpose_cl character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sosoilsitepolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    soilinvestigationpurpose character varying,
    soilinvestigationpurpose_cl character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srcoastline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    waterlevel character varying,
    waterlevel_cl character varying,
    waterlevel_void character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    id_namedplace_geographicalname character varying,
    id_coastline character varying,
    shape_length double precision
);

CREATE TABLE srcoastline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    waterlevel character varying,
    waterlevel_cl character varying,
    waterlevel_void character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    id_namedplace_geographicalname character varying,
    id_coastline character varying,
    shape_length double precision
);

CREATE TABLE srintertidalarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    composition character varying,
    composition_cl character varying,
    composition_void character varying,
    delineationknown character varying,
    delineationknown_void character varying,
    highwaterlevel character varying,
    lowwaterlevel character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srintertidalarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    composition character varying,
    composition_cl character varying,
    composition_void character varying,
    delineationknown character varying,
    delineationknown_void character varying,
    highwaterlevel character varying,
    lowwaterlevel character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srmarinecirculationzone_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    seaareatype character varying,
    seaareatype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    subarea integer,
    marineextent_waterlevel character varying,
    zonetype character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srmarinecirculationzone_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    seaareatype character varying,
    seaareatype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    subarea integer,
    marineextent_waterlevel character varying,
    zonetype character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srmarinecontour_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    validtime character varying,
    id_marinecontour character varying,
    id_abstractobservableproperty character varying,
    shape_length double precision
);

CREATE TABLE srmarinecontour_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    validtime character varying,
    id_marinecontour character varying,
    id_abstractobservableproperty character varying,
    shape_length double precision
);

CREATE TABLE srmarineextent_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    waterlevel character varying,
    waterlevel_cl character varying,
    id_seaarea character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srmarineextent_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    waterlevel character varying,
    waterlevel_cl character varying,
    id_seaarea character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srsea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    seaareatype character varying,
    seaareatype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject character varying,
    relatedhydroobject_void character varying,
    subarea character varying,
    marineextent_waterlevel character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srsea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    seaareatype character varying,
    seaareatype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    subarea integer,
    marineextent_waterlevel character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srseaarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    seaareatype character varying,
    seaareatype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    subarea integer,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srseaarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    seaareatype character varying,
    seaareatype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    subarea integer,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE srshoreline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    waterlevel character varying,
    waterlevel_cl character varying,
    waterlevel_void character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    id_namedplace_geographicalname character varying,
    id_shoreline character varying,
    shape_length double precision
);

CREATE TABLE srshoreline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    waterlevel character varying,
    waterlevel_cl character varying,
    waterlevel_void character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    id_namedplace_geographicalname character varying,
    id_shoreline character varying,
    shape_length double precision
);

CREATE TABLE srshoresegment_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    shoreclassification character varying,
    shoreclassification_cl character varying,
    shoreclassification_void character varying,
    shorestability character varying,
    shorestability_cl character varying,
    shorestability_void character varying,
    id_feature character varying,
    shape_length double precision
);

CREATE TABLE srshoresegment_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    shoreclassification character varying,
    shoreclassification_cl character varying,
    shoreclassification_void character varying,
    shorestability character varying,
    shorestability_cl character varying,
    shorestability_void character varying,
    id_feature character varying,
    shape_length double precision
);

CREATE TABLE suareastatisticalunit_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    thematicid character varying,
    country character varying,
    geographicalname character varying,
    validityperiod timestamp with time zone,
    referenceperiod timestamp with time zone,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    areavalue double precision,
    landareavalue double precision,
    landareavalue_void character varying,
    livableareavalue double precision,
    livableareavalue_void character varying,
    evolutions character varying,
    evolutions_void character varying,
    lowers integer,
    lowers_void character varying,
    upper integer,
    upper_void character varying,
    predecessors integer,
    predecessors_void character varying,
    successors integer,
    successors_void character varying,
    id_statisticaltessellation character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE suareastatisticalunit_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    thematicid character varying,
    country character varying,
    geographicalname character varying,
    validityperiod timestamp with time zone,
    referenceperiod timestamp with time zone,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    areavalue double precision,
    landareavalue double precision,
    landareavalue_void character varying,
    livableareavalue double precision,
    livableareavalue_void character varying,
    evolutions character varying,
    evolutions_void character varying,
    lowers integer,
    lowers_void character varying,
    upper integer,
    upper_void character varying,
    predecessors integer,
    predecessors_void character varying,
    successors integer,
    successors_void character varying,
    id_statisticaltessellation character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sustatisticalgrid_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    epsgcode integer,
    origin character varying,
    width integer,
    height integer,
    resolution_lengthresolution double precision,
    resolution_angleresolution integer,
    lowers integer,
    lowers_void character varying,
    upper integer,
    upper_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sustatisticalgrid_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    epsgcode integer,
    origin character varying,
    width integer,
    height integer,
    resolution_lengthresolution character varying,
    resolution_angleresolution character varying,
    lowers integer,
    lowers_void character varying,
    upper integer,
    upper_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sustatisticalgridcell_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    code character varying,
    code_void character varying,
    geographicalposition character varying,
    geographicalposition_void character varying,
    gridposition_x integer,
    gridposition_x_void character varying,
    gridposition_y integer,
    gridposition_y_void character varying,
    lowers integer,
    lowers_void character varying,
    upper integer,
    upper_void character varying,
    id_statisticalgrid character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sustatisticalgridcell_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    code character varying,
    code_void character varying,
    geographicalposition character varying,
    geographicalposition_void character varying,
    gridposition_x integer,
    gridposition_x_void character varying,
    gridposition_y integer,
    gridposition_y_void character varying,
    lowers integer,
    lowers_void character varying,
    upper integer,
    upper_void character varying,
    id_statisticalgrid character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sustatisticaltessellation_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    lower integer,
    lower_void character varying,
    upper integer,
    upper_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE sustatisticaltessellation_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    lower integer,
    lower_void character varying,
    upper integer,
    upper_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE suvectorstatisticalunitline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geometrydescriptor_geometrytype character varying,
    geometrydescriptor_geometrytype_cl character varying,
    geometrydescriptor_mostdetailedscale integer,
    geometrydescriptor_leastdetailedscale integer,
    id_vectorstatisticalunit character varying,
    shape_length double precision
);

CREATE TABLE suvectorstatisticalunitline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geometrydescriptor_geometrytype character varying,
    geometrydescriptor_geometrytype_cl character varying,
    geometrydescriptor_mostdetailedscale integer,
    geometrydescriptor_leastdetailedscale integer,
    id_vectorstatisticalunit character varying,
    shape_length double precision
);

CREATE TABLE suvectorstatisticalunitpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geometrydescriptor_geometrytype character varying,
    geometrydescriptor_geometrytype_cl character varying,
    geometrydescriptor_mostdetailedscale integer,
    geometrydescriptor_leastdetailedscale integer,
    id_vectorstatisticalunit character varying
);

CREATE TABLE suvectorstatisticalunitpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geometrydescriptor_geometrytype character varying,
    geometrydescriptor_geometrytype_cl character varying,
    geometrydescriptor_mostdetailedscale integer,
    geometrydescriptor_leastdetailedscale integer,
    id_vectorstatisticalunit character varying
);

CREATE TABLE t_activitycomplex (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    validfrom timestamp with time zone,
    validto timestamp with time zone,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    name_void character varying,
    validfrom_void character varying,
    validto_void character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying,
    id_feature character varying
);

CREATE TABLE t_activitycomplex_capacity (
    ogc_fid SERIAL PRIMARY KEY,
    input_ character varying,
    output_ character varying,
    time_ timestamp with time zone,
    description character varying,
    description_void character varying,
    id_feature character varying
);

CREATE TABLE t_activitycomplex_description (
    ogc_fid SERIAL PRIMARY KEY,
    description character varying,
    description_void character varying,
    address_void character varying,
    contact_void character varying,
    relatedparty_void character varying,
    id_feature character varying
);

CREATE TABLE t_activitycomplex_economicactivityvalue (
    ogc_fid SERIAL PRIMARY KEY,
    activity character varying,
    id_feature character varying
);

CREATE TABLE t_activitycomplex_function (
    ogc_fid SERIAL PRIMARY KEY,
    input_ character varying,
    output_ character varying,
    description character varying,
    input_void character varying,
    output_void character varying,
    description_void character varying,
    id_feature character varying
);

CREATE TABLE t_activitycomplex_inputoutputamount (
    ogc_fid SERIAL PRIMARY KEY,
    inputoutput character varying,
    amount_value character varying,
    amount_uom character varying,
    amount_void character varying,
    id_feature character varying
);

CREATE TABLE t_activitycomplex_inputoutputvalue (
    ogc_fid SERIAL PRIMARY KEY,
    inputoutputvalue character varying,
    id_feature character varying
);

CREATE TABLE t_activitycomplex_permission (
    ogc_fid SERIAL PRIMARY KEY,
    decisiondate timestamp with time zone,
    datefrom timestamp with time zone,
    dateto timestamp with time zone,
    description character varying,
    relatedparty_void character varying,
    decisiondate_void character varying,
    datefrom_void character varying,
    dateto_void character varying,
    description_void character varying,
    permittedfunction_void character varying,
    permittedcapacity_void character varying,
    id_feature character varying,
    id_permission character varying
);

CREATE TABLE t_ad_address (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    
    
    status character varying,
    status_cl character varying,
    status_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    building_void character varying,
    parcel_void character varying,
    parentaddress integer,
    parentaddress_void character varying
);

CREATE TABLE t_ad_addressareaname (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    
    
    status character varying,
    status_cl character varying,
    status_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    building_void character varying,
    parcel_void character varying,
    parentaddress integer,
    parentaddress_void character varying,
    situatedwithin integer,
    situatedwithin_void character varying,
    id_namedplace_geographicalname character varying,
    id_address character varying,
    id_namedplace character varying,
    id_addresslocator character varying
);

CREATE TABLE t_ad_addresslocator (
    ogc_fid SERIAL PRIMARY KEY,
    level_ character varying,
    level_cl character varying,
    id_addresslocator character varying,
    id_address character varying
);

CREATE TABLE t_ad_addressrepresentation (
    ogc_fid SERIAL PRIMARY KEY,
    addressarea_void character varying,
    postname_void character varying,
    postcode character varying,
    postcode_void character varying,
    thoroughfare_void character varying,
    id_addressrepresentation character varying,
    id_address character varying,
    id_building character varying,
    id_geographicalname_adminunit character varying,
    id_geographicalname_locatorname character varying,
    id_geographicalname_addressarea character varying,
    id_geographicalname_postname character varying,
    id_geographicalname_tthoroughfare character varying
);

CREATE TABLE t_ad_addressrepresentation_locatordesignator (
    ogc_fid SERIAL PRIMARY KEY,
    locatordesignator character varying,
    id_addressrepresentation character varying
);

CREATE TABLE t_ad_adminunitname (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    
    
    status character varying,
    status_cl character varying,
    status_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    building_void character varying,
    parcel_void character varying,
    parentaddress integer,
    parentaddress_void character varying,
    level_ character varying,
    situatedwithin integer,
    situatedwithin_void character varying,
    id_namedplace_geographicalname character varying,
    id_address character varying,
    id_administrativeunit character varying,
    id_addresslocator character varying
);

CREATE TABLE t_ad_locatordesignator (
    ogc_fid SERIAL PRIMARY KEY,
    designator character varying,
    type character varying,
    type_cl character varying,
    id_addresslocator character varying
);

CREATE TABLE t_ad_locatorname (
    ogc_fid SERIAL PRIMARY KEY,
    type character varying,
    type_cl character varying,
    id_namedplace_geographicalname character varying,
    id_addresslocator character varying
);

CREATE TABLE t_ad_partofname (
    ogc_fid SERIAL PRIMARY KEY,
    part character varying,
    type character varying,
    type_cl character varying,
    type_void character varying,
    id_thoroughfarenamevalue character varying
);

CREATE TABLE t_ad_postaldescriptor (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    
    
    status character varying,
    status_cl character varying,
    status_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    building_void character varying,
    parcel_void character varying,
    parentaddress integer,
    parentaddress_void character varying,
    postcode character varying,
    situatedwithin double precision,
    situatedwithin_void character varying,
    id_namedplace_geographicalname character varying,
    id_address character varying,
    id_addresslocator character varying
);

CREATE TABLE t_ad_thoroughfarename (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    
    
    status character varying,
    status_cl character varying,
    status_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    building_void character varying,
    parcel_void character varying,
    parentaddress integer,
    parentaddress_void character varying,
    postcode character varying,
    situatedwithin integer,
    situatedwithin_void character varying,
    id_address character varying,
    id_addresslocator character varying
);

CREATE TABLE t_ad_thoroughfarenamevalue (
    ogc_fid SERIAL PRIMARY KEY,
    nameparts_void character varying,
    id_namedplace_geographicalname character varying,
    id_thoroughfarename character varying,
    id_thoroughfarenamevalue character varying
);

CREATE TABLE t_am_environmentaldomain (
    ogc_fid SERIAL PRIMARY KEY,
    environmentaldomain character varying,
    environmentaldomain_cl character varying,
    id_managementrestrictionorregulationzone character varying
);

CREATE TABLE t_am_zonetypecode (
    ogc_fid SERIAL PRIMARY KEY,
    zonetype character varying,
    zonetype_cl character varying,
    id_managementrestrictionorregulationzone character varying
);

CREATE TABLE t_au_administrativeboundary_nationallevel (
    ogc_fid SERIAL PRIMARY KEY,
    nationallevel character varying,
    nationallevel_cl character varying,
    id_administrativeboundary character varying
);

CREATE TABLE t_au_administrativeunit_nationallevelname (
    ogc_fid SERIAL PRIMARY KEY,
    nationallevelname character varying,
    nationallevelname_local character varying,
    id_administrativeunit character varying
);

CREATE TABLE t_au_maritimeboundary_country (
    ogc_fid SERIAL PRIMARY KEY,
    country character varying,
    id_maritimeboundary character varying
);

CREATE TABLE t_bu_buildingnature (
    ogc_fid SERIAL PRIMARY KEY,
    buildingnaturevalue character varying,
    buildingnaturevalue_cl character varying,
    id_abstratctconstruction character varying
);

CREATE TABLE t_bu_currentuse (
    ogc_fid SERIAL PRIMARY KEY,
    currentusevalue character varying,
    currentusevalue_cl character varying,
    percentage integer,
    percentage_void character varying,
    id_abstractconstruction_buildingunit character varying
);

CREATE TABLE t_bu_dateofconstruction (
    ogc_fid SERIAL PRIMARY KEY,
    anypoint timestamp with time zone,
    anypoint_void character varying,
    beginning timestamp with time zone,
    beginning_void character varying,
    end_ timestamp with time zone,
    end_void character varying,
    id_abstratctconstruction character varying
);

CREATE TABLE t_bu_dateofdemolition (
    ogc_fid SERIAL PRIMARY KEY,
    anypoint timestamp with time zone,
    anypoint_void character varying,
    beginning timestamp with time zone,
    beginning_void character varying,
    end_ timestamp with time zone,
    end_void character varying,
    id_abstratctconstruction character varying
);

CREATE TABLE t_bu_dateofrenovation (
    ogc_fid SERIAL PRIMARY KEY,
    anypoint timestamp with time zone,
    anypoint_void character varying,
    beginning timestamp with time zone,
    beginning_void character varying,
    end_ timestamp with time zone,
    end_void character varying,
    id_abstratctconstruction character varying
);

CREATE TABLE t_bu_document_extended (
    ogc_fid SERIAL PRIMARY KEY,
    documentlink character varying,
    date_ timestamp with time zone,
    date_void character varying,
    documentdescription character varying,
    documentdescription_void character varying,
    sourcestatus character varying,
    sourcestatus_void character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_elevation (
    ogc_fid SERIAL PRIMARY KEY,
    elevationreference character varying,
    elevationreference_cl character varying,
    elevationvalue character varying,
    id_abstractconstruction character varying
);

CREATE TABLE t_bu_energyperformance_extended (
    ogc_fid SERIAL PRIMARY KEY,
    energyperformancevalue character varying,
    dateofassessment timestamp with time zone,
    dateofassessment_void character varying,
    assessmentmethod_void character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_externalreference (
    ogc_fid SERIAL PRIMARY KEY,
    informationsystem character varying,
    informationsystemname character varying,
    reference character varying,
    id_abstractconstruction_buildingunit character varying
);

CREATE TABLE t_bu_floordescription_extended (
    ogc_fid SERIAL PRIMARY KEY,
    areaofopenings double precision,
    floorarea double precision,
    height double precision,
    numberofdwellings integer,
    areaofopenings_void character varying,
    currentuse_void character varying,
    document_void character varying,
    floorarea_void character varying,
    height_void character varying,
    numberofdwellings_void character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_floordistribution_extended (
    ogc_fid SERIAL PRIMARY KEY,
    floorrange character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_floorrange_extended (
    ogc_fid SERIAL PRIMARY KEY,
    lowestfloor character varying,
    highestfloor character varying,
    id_floor character varying
);

CREATE TABLE t_bu_heatingsource_extended (
    ogc_fid SERIAL PRIMARY KEY,
    heatingsource character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_heatingsystem_extended (
    ogc_fid SERIAL PRIMARY KEY,
    heatingsystem character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_heightaboveground (
    ogc_fid SERIAL PRIMARY KEY,
    value_ double precision,
    heightreference character varying,
    heightreference_cl character varying,
    heightreference_void character varying,
    lowreference character varying,
    lowreference_cl character varying,
    lowreference_void character varying,
    status character varying,
    status_void character varying,
    id_abstractconstruction character varying
);

CREATE TABLE t_bu_materialoffacade_extended (
    ogc_fid SERIAL PRIMARY KEY,
    materialoffacade character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_materialofroof_extended (
    ogc_fid SERIAL PRIMARY KEY,
    materialofroof character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_materialofstructure_extended (
    ogc_fid SERIAL PRIMARY KEY,
    materialofstructure character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_officialarea_extended (
    ogc_fid SERIAL PRIMARY KEY,
    value_ double precision,
    officialareareference character varying,
    heightparameter character varying,
    officialareareference_void character varying,
    heightparameter_void character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_officialvalue_extended (
    ogc_fid SERIAL PRIMARY KEY,
    currency_ character varying,
    value_ integer,
    valuationdate timestamp with time zone,
    officialvaluereference character varying,
    referencepercentage integer,
    informationsystemname character varying,
    currency_void character varying,
    value_void character varying,
    valuationdate_void character varying,
    officialvaluereference_void character varying,
    referencepercentage_void character varying,
    informationsystemname_void character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_bu_rooftype_extended (
    ogc_fid SERIAL PRIMARY KEY,
    rooftype character varying,
    id_buildingandbuildingunitinfo character varying
);

CREATE TABLE t_ci_
    ogc_fid SERIAL PRIMARY KEY,
    
    id_citation character varying
);

CREATE TABLE t_ci_citation (
    ogc_fid SERIAL PRIMARY KEY,
    collectivetitle character varying,
    date_ timestamp with time zone,
    datetype character varying,
    edition character varying,
    editiondate character varying,
    isbn character varying,
    issn character varying,
    othercitationdetails character varying,
    title character varying,
    id_feature character varying
);

CREATE TABLE t_ci_presentationform (
    ogc_fid SERIAL PRIMARY KEY,
    presentationformcode character varying,
    id_citation character varying
);

CREATE TABLE t_ci_series (
    ogc_fid SERIAL PRIMARY KEY,
    issueidentification character varying,
    name character varying,
    page character varying,
    id_citation character varying
);

CREATE TABLE t_contact (
    ogc_fid SERIAL PRIMARY KEY,
    address_void character varying,
    contactinstructions character varying,
    contactinstructions_void character varying,
    electronicmailaddress character varying,
    electronicmailaddress_void character varying,
    hoursofservice character varying,
    hoursofservice_void character varying,
    telephonefacsimile character varying,
    telephonefacsimile_void character varying,
    telephonevoice character varying,
    telephonevoice_void character varying,
    website character varying,
    website_void character varying,
    id_relatedparty character varying,
    id_feature character varying
);

CREATE TABLE t_cp_basicpropertyunit (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    nationalcadastralreference character varying,
    areavalue double precision,
    areavalue_uom character varying,
    areavalue_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    adminunit integer,
    adminunit_void character varying,
    id_cadastralparcel character varying
);

CREATE TABLE t_cp_cadastralzoning_levelname (
    ogc_fid SERIAL PRIMARY KEY,
    levelname character varying,
    levelname_local character varying,
    id_cadastralzoning character varying
);

CREATE TABLE t_date (
    ogc_fid SERIAL PRIMARY KEY,
    date_ timestamp with time zone,
    datetype character varying,
    datetype_cl character varying,
    id_documentcitation character varying
);

CREATE TABLE t_documentcitation (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    shortname character varying,
    shortname_void character varying,
    date_void character varying,
    link_void character varying,
    id_documentcitation character varying,
    id_feature character varying
);

CREATE TABLE t_documentcitation_link (
    ogc_fid SERIAL PRIMARY KEY,
    link character varying,
    id_documentcitation character varying
);

CREATE TABLE t_documentcitation_specificreference (
    ogc_fid SERIAL PRIMARY KEY,
    specificreference character varying,
    id_documentcitation character varying
);

CREATE TABLE t_ef_abstractmonitoringobject_mediavalue (
    ogc_fid SERIAL PRIMARY KEY,
    mediamonitored character varying,
    mediamonitored_cl character varying,
    id_abstractmonitoringobject character varying
);

CREATE TABLE t_ef_abstractmonitoringobject_name (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    id_abstractmonitoringobject character varying
);

CREATE TABLE t_ef_abstractmonitoringobject_purposeofcollectionvalue (
    ogc_fid SERIAL PRIMARY KEY,
    purpose character varying,
    purpose_cl character varying,
    id_abstractmonitoringobject character varying
);

CREATE TABLE t_ef_abstractmonitoringobject_url (
    ogc_fid SERIAL PRIMARY KEY,
    onlineresource character varying,
    id_abstractmonitoringobject character varying
);

CREATE TABLE t_ef_anydomainlink (
    ogc_fid SERIAL PRIMARY KEY,
    comment character varying,
    id_environmentalmonitoringfacility character varying
);

CREATE TABLE t_ef_environmentalmonitoringactivity_url (
    ogc_fid SERIAL PRIMARY KEY,
    onlineresource character varying,
    id_environmentalmonitoringactivity character varying
);

CREATE TABLE t_ef_envmonitoringfacility_resultacquisitionsourcevalue (
    ogc_fid SERIAL PRIMARY KEY,
    resultacquisitionsource character varying,
    resultacquisitionsource_cl character varying,
    id_abstractmonitoringobject character varying
);

CREATE TABLE t_ef_hierarchy (
    ogc_fid SERIAL PRIMARY KEY,
    linkingtime character varying,
    id_abstractmonitoringobject character varying
);

CREATE TABLE t_ef_observingcapability (
    ogc_fid SERIAL PRIMARY KEY,
    observingtime character varying,
    observingtime_void character varying,
    processtype character varying,
    processtype_cl character varying,
    processtype_void character varying,
    resultnature character varying,
    resultnature_cl character varying,
    resultnature_void character varying,
    onlineresource character varying,
    onlineresource_void character varying,
    featureofinterest_void character varying,
    id_observingcapability character varying,
    id_abstractmonitoringobject character varying,
    id_featureofinterest character varying,
    id_process character varying
);

CREATE TABLE t_el_chartdatum (
    ogc_fid SERIAL PRIMARY KEY,
    datumwaterlevel character varying,
    scope character varying,
    id_elevationvectorobject character varying
);

CREATE TABLE t_el_chartdatum_offset (
    ogc_fid SERIAL PRIMARY KEY,
    "offset" double precision,
    offset_uom character varying,
    id_chartdatum character varying
);

CREATE TABLE t_er_calorificvaluetype (
    ogc_fid SERIAL PRIMARY KEY,
    calorificrange_lowerbound character varying,
    calorificrange_lowerbound_uom character varying,
    calorificrange_upperbound character varying,
    calorificrange_upperbound_uom character varying,
    calorificscalar character varying,
    calorificscalar_uom character varying,
    id_fossilfuelresourcetype character varying
);

CREATE TABLE t_er_ex_extent (
    ogc_fid SERIAL PRIMARY KEY,
    domainextent character varying,
    id_renewableandwastepotentialcoverage character varying
);

CREATE TABLE t_er_exploitationperiodtype (
    ogc_fid SERIAL PRIMARY KEY,
    begintime timestamp with time zone,
    endtime timestamp with time zone,
    id_vectorenergyresource character varying
);

CREATE TABLE t_er_fossilfuelmeasure (
    ogc_fid SERIAL PRIMARY KEY,
    amount character varying,
    amount_uom character varying,
    dateofdetermination timestamp with time zone,
    resourceclass character varying,
    resourceclass_cl character varying,
    id_fossilfuelresourcetype character varying
);

CREATE TABLE t_er_fossilfuelresourcetype (
    ogc_fid SERIAL PRIMARY KEY,
    calorificvalue_void character varying,
    quantity_void character varying,
    typeofresource character varying,
    typeofresource_cl character varying,
    id_fossilfuelresource character varying,
    id_fossilfuelresourcetype character varying
);

CREATE TABLE t_er_verticalextenttype (
    ogc_fid SERIAL PRIMARY KEY,
    verticalextent character varying,
    verticalextent_cl character varying,
    verticalreference character varying,
    verticalreference_cl character varying,
    id_vectorenergyresource character varying,
    id_renewableandwastepotentialcoverage character varying
);

CREATE TABLE t_ge_activewelltypevalue (
    ogc_fid SERIAL PRIMARY KEY,
    activewelltypevalue character varying,
    activewelltypevalue_cl character varying,
    id_activewell character varying
);

CREATE TABLE t_ge_anthropogenicgeomorphologicfeature (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    anthropogenicgeomorphologicfeaturetype character varying,
    anthropogenicgeomorphologicfeaturetype_cl character varying
);

CREATE TABLE t_ge_aquiclude (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    geologicunittype character varying,
    geologicunittype_cl character varying,
    description character varying,
    description_void character varying,
    approximatedepth_singlequantity double precision,
    approximatedepth_quantityinterval character varying,
    approximatedepth_void character varying,
    approximatethickness_singlequantity double precision,
    approximatethickness_quantityinterval character varying,
    approximatethickness_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_aquifersystem character varying
);

CREATE TABLE t_ge_aquifer (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    geologicunittype character varying,
    geologicunittype_cl character varying,
    description character varying,
    description_void character varying,
    approximatedepth_singlequantity double precision,
    approximatedepth_quantityinterval character varying,
    approximatedepth_void character varying,
    approximatethickness_singlequantity double precision,
    approximatethickness_quantityinterval character varying,
    approximatethickness_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    aquifertype character varying,
    aquifertype_cl character varying,
    mediatype character varying,
    mediatype_cl character varying,
    isexploited character varying,
    ismaininsystem character varying,
    vulnerabilitytopollution_quantity double precision,
    vulnerabilitytopollution_quantityrange character varying,
    permeabilitycoefficient_quantity double precision,
    permeabilitycoefficient_quantityrange character varying,
    storativitycoefficient_quantity double precision,
    storativitycoefficient_quantityrange character varying,
    hydrogeochemicalrocktype character varying,
    isexploited_void character varying,
    ismaininsystem_void character varying,
    vulnerabilitytopollution_void character varying,
    permeabilitycoefficient_void character varying,
    storativitycoefficient_void character varying,
    hydrogeochemicalrocktype_void character varying,
    id_aquifersystem character varying
);

CREATE TABLE t_ge_aquifersystem (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    geologicunittype character varying,
    geologicunittype_cl character varying,
    description character varying,
    description_void character varying,
    approximatedepth_singlequantity double precision,
    approximatedepth_quantityinterval character varying,
    approximatedepth_void character varying,
    approximatethickness_singlequantity double precision,
    approximatethickness_quantityinterval character varying,
    approximatethickness_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    islayered character varying,
    islayered_void character varying
);

CREATE TABLE t_ge_aquitard (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    geologicunittype character varying,
    geologicunittype_cl character varying,
    description character varying,
    description_void character varying,
    approximatedepth_singlequantity double precision,
    approximatedepth_quantityinterval character varying,
    approximatedepth_void character varying,
    approximatethickness_singlequantity double precision,
    approximatethickness_quantityinterval character varying,
    approximatethickness_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    approximatepermeabilitycoeficient_singlequantity double precision,
    approximatepermeabilitycoeficient_quantityinterval character varying,
    approximatepermeabilitycoeficient_void character varying,
    approximatestorativitycoeficient_singlequantity double precision,
    approximatestorativitycoeficient_quantityinterval character varying,
    approximatestorativitycoeficient_void character varying,
    id_aquifersystem character varying
);

CREATE TABLE t_ge_borehole (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    boreholelength double precision,
    boreholelength_void character varying,
    elevation double precision,
    elevation_void character varying,
    downholegeometry_void character varying,
    purpose_void character varying,
    id_activewell character varying,
    id_geologiccollection character varying
);

CREATE TABLE t_ge_boreholepurposevalue (
    ogc_fid SERIAL PRIMARY KEY,
    boreholepurposevalue character varying,
    boreholepurposevalue_cl character varying,
    boreholepurposevalue_void character varying,
    id_borehole character varying
);

CREATE TABLE t_ge_compositionpart (
    ogc_fid SERIAL PRIMARY KEY,
    material character varying,
    material_cl character varying,
    role character varying,
    role_cl character varying,
    proportion character varying,
    proportion_void character varying,
    id_geologicunit character varying
);

CREATE TABLE t_ge_fold (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    profiletype character varying,
    profiletype_cl character varying,
    id_hydrogeologicunit character varying
);

CREATE TABLE t_ge_geologiccollection (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    collectiontype character varying,
    collectiontype_cl character varying,
    beginlifespanversion timestamp with time zone,
    endlifespanversion timestamp with time zone,
    reference_void character varying,
    beginlifespanversion_void character varying,
    endlifespanversion_void character varying
);

CREATE TABLE t_ge_geologicevent (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    name_void character varying,
    eventenvironment character varying,
    eventenvironment_cl character varying,
    eventenvironment_void character varying,
    oldernamedage character varying,
    oldernamedage_cl character varying,
    oldernamedage_void character varying,
    youngernamedage character varying,
    youngernamedage_cl character varying,
    youngernamedage_void character varying,
    eventprocess_void character varying,
    id_geologicfeature character varying
);

CREATE TABLE t_ge_geologicevent_eventprocessvalue (
    ogc_fid SERIAL PRIMARY KEY,
    eventprocessvalue character varying,
    eventprocessvalue_cl character varying,
    eventprocessvalue_void character varying,
    id_geologicevent character varying
);

CREATE TABLE t_ge_geologicunit (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    geologicunittype character varying,
    geologicunittype_cl character varying
);

CREATE TABLE t_ge_largerwork (
    ogc_fid SERIAL PRIMARY KEY,
    namespace character varying,
    localid character varying,
    versionid character varying,
    id_geophobject character varying
);

CREATE TABLE t_ge_naturalgeomorphologicfeature (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    naturalgeomorphologicfeaturetype character varying,
    naturalgeomorphologicfeaturetype_cl character varying,
    activity character varying,
    activity_cl character varying,
    activity_void character varying
);

CREATE TABLE t_ge_networknamevalue (
    ogc_fid SERIAL PRIMARY KEY,
    networknamevalue character varying,
    networknamevalue_cl character varying,
    id_geophmeasurement character varying
);

CREATE TABLE t_ge_relatedmodel (
    ogc_fid SERIAL PRIMARY KEY,
    namespace character varying,
    localid character varying,
    versionid character varying,
    id_geophmeasurement character varying
);

CREATE TABLE t_ge_sheardisplacementstructure (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    faulttype character varying,
    faulttype_cl character varying,
    id_hydrogeologicunit character varying
);

CREATE TABLE t_ge_stationrankvalue (
    ogc_fid SERIAL PRIMARY KEY,
    stationrankvalue character varying,
    stationrankvalue_cl character varying,
    stationrankvalue_void character varying,
    id_geophstation character varying
);

CREATE TABLE t_ge_thematicclass (
    ogc_fid SERIAL PRIMARY KEY,
    themeclassification character varying,
    themeclassification_cl character varying,
    themeclass character varying,
    themeclass_cl character varying,
    id_geologicfeature character varying
);

CREATE TABLE t_gf_propertytype (
    ogc_fid SERIAL PRIMARY KEY,
    definition character varying,
    membername character varying,
    id_feature character varying
);

CREATE TABLE t_gn_geographicalname (
    ogc_fid SERIAL PRIMARY KEY,
    language_ character varying,
    language_void character varying,
    nativeness character varying,
    nativeness_cl character varying,
    nativeness_void character varying,
    namestatus character varying,
    namestatus_cl character varying,
    namestatus_void character varying,
    sourceofname character varying,
    sourceofname_void character varying,
    pronunciation_void character varying,
    grammaticalgender character varying,
    grammaticalgender_cl character varying,
    grammaticalgender_void character varying,
    grammaticalnumber character varying,
    grammaticalnumber_cl character varying,
    grammaticalnumber_void character varying,
    id_geographicalname character varying,
    id_namedplace character varying
);

CREATE TABLE t_gn_namedplace_localtype (
    ogc_fid SERIAL PRIMARY KEY,
    localtype character varying,
    localtype_local character varying,
    id_namedplace character varying
);

CREATE TABLE t_gn_namedplace_relatedspatialobject (
    ogc_fid SERIAL PRIMARY KEY,
    id_namedplace character varying,
    relatedspatialobject character varying
);

CREATE TABLE t_gn_namedplace_type (
    ogc_fid SERIAL PRIMARY KEY,
    type character varying,
    type_cl character varying,
    id_namedplace character varying
);

CREATE TABLE t_gn_pronunciationofname (
    ogc_fid SERIAL PRIMARY KEY,
    pronunciationsoundlink character varying,
    pronunciationsoundlink_void character varying,
    pronunciationipa character varying,
    pronunciationipa_void character varying,
    id_geographicalname character varying
);

CREATE TABLE t_gn_spellingofname (
    ogc_fid SERIAL PRIMARY KEY,
    text_ character varying,
    script character varying,
    script_void character varying,
    transliterationscheme character varying,
    transliterationscheme_void character varying,
    id_geographicalname character varying
);

CREATE TABLE t_hb_habitatspeciestype (
    ogc_fid SERIAL PRIMARY KEY,
    referencespeciesid character varying,
    referencespeciesid_cl character varying,
    referencespeciesscheme character varying,
    referencespeciesscheme_cl character varying,
    localspeciesname_void character varying,
    id_habitat character varying,
    id_habitatspeciestype character varying
);

CREATE TABLE t_hb_habitattypecovertype (
    ogc_fid SERIAL PRIMARY KEY,
    referencehabitattypeid character varying,
    referencehabitattypeid_cl character varying,
    referencehabitattypescheme character varying,
    referencehabitattypescheme_cl character varying,
    referencehabitattypename character varying,
    referencehabitattypename_void character varying,
    localhabitatname_void character varying,
    areacovered_uom character varying,
    areacovered_void character varying,
    lengthcovered character varying,
    lengthcovered_uom character varying,
    lengthcovered_void character varying,
    volumecovered character varying,
    volumecovered_uom character varying,
    volumecovered_void character varying,
    id_habitat character varying,
    id_habitattypecovertype character varying
);

CREATE TABLE t_hb_habitatvegetationtype (
    ogc_fid SERIAL PRIMARY KEY,
    id_habitatvegetationtype character varying,
    id_habitat character varying
);

CREATE TABLE t_hb_localnametype (
    ogc_fid SERIAL PRIMARY KEY,
    localscheme character varying,
    localnamecode character varying,
    localnamecode_cl character varying,
    localname character varying,
    localname_void character varying,
    qualifierlocalname character varying,
    qualifierlocalname_cl character varying,
    qualifierlocalname_void character varying,
    id_feature character varying
);

CREATE TABLE t_hh_agerangetype (
    ogc_fid SERIAL PRIMARY KEY,
    startage_month integer,
    startage_week integer,
    startage_year integer,
    range_month integer,
    range_week integer,
    range_year integer,
    id_feature character varying
);

CREATE TABLE t_hh_biomarker (
    ogc_fid SERIAL PRIMARY KEY,
    gender character varying,
    aggregationunit character varying,
    id_biomarker character varying,
    id_biomarkerthematicmetadata character varying,
    id_statisticalunit character varying
);

CREATE TABLE t_hh_biomarkerstatisticalparametertype (
    ogc_fid SERIAL PRIMARY KEY,
    geometricmean_value double precision,
    geometricmean_measuretype character varying,
    ci95ofgm_value double precision,
    ci95ofgm_measuretype character varying,
    p50_value double precision,
    p50_measuretype character varying,
    p90_value double precision,
    p90_measuretype character varying,
    p95_value double precision,
    p95_measuretype character varying,
    ci95ofp95_value double precision,
    ci95ofp95_measuretype character varying,
    maximum_value double precision,
    maximum_measuretype character varying,
    numberofpartecipants integer,
    pinlod double precision,
    loq double precision,
    id_biomarker character varying
);

CREATE TABLE t_hh_biomarkerthematicmetadata (
    ogc_fid SERIAL PRIMARY KEY,
    studytype character varying,
    areatype character varying,
    specificsubpopulation character varying,
    meanage_month integer,
    meanage_week integer,
    meanage_year integer,
    id_biomarkerthematicmetadata character varying
);

CREATE TABLE t_hh_biomarkertype (
    ogc_fid SERIAL PRIMARY KEY,
    chemical character varying,
    chemical_cl character varying,
    matrix character varying,
    id_biomarker character varying
);

CREATE TABLE t_hh_disease (
    ogc_fid SERIAL PRIMARY KEY,
    pathology character varying,
    pathology_cl character varying,
    cod character varying,
    cod_cl character varying,
    gender character varying,
    gender_void character varying,
    agerange_void character varying,
    aggregationunit character varying,
    id_disease character varying,
    id_statisticalunit character varying
);

CREATE TABLE t_hh_diseasemeasure (
    ogc_fid SERIAL PRIMARY KEY,
    diseasemeasuretype character varying,
    diseasemeasuretype_cl character varying,
    value_ double precision,
    id_disease character varying
);

CREATE TABLE t_hh_generalhealthstatistics (
    ogc_fid SERIAL PRIMARY KEY,
    generalhealthname character varying,
    generalhealthname_cl character varying,
    generalhealthvalue double precision,
    gender character varying,
    gender_cl character varying,
    agerange_void character varying,
    gender_void character varying,
    aggregationunit character varying,
    id_generalhealthstatistics character varying,
    id_statisticalunit character varying
);

CREATE TABLE t_hh_healthservicesstatistic (
    ogc_fid SERIAL PRIMARY KEY,
    healthservicetype character varying,
    healthservicetype_cl character varying,
    healthservicevalue double precision,
    aggregationunit character varying,
    id_healthservicesstatistic character varying,
    id_statisticalunit character varying
);

CREATE TABLE t_hh_referenceperiodtype (
    ogc_fid SERIAL PRIMARY KEY,
    startdate timestamp with time zone,
    enddate timestamp with time zone,
    id_feature character varying
);

CREATE TABLE t_hy_directedlink (
    ogc_fid SERIAL PRIMARY KEY,
    direction character varying,
    id_linksequence character varying,
    id_link character varying
);

CREATE TABLE t_hy_hydroidentifier (
    ogc_fid SERIAL PRIMARY KEY,
    namespace character varying,
    localid character varying,
    hydroid character varying,
    classificationscheme character varying
);

CREATE TABLE t_hy_hydroordercode (
    ogc_fid SERIAL PRIMARY KEY,
    order_ character varying,
    orderscheme character varying,
    scope character varying,
    inspireid character varying
);

CREATE TABLE t_hy_watercourseseparatedcrossing (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    geographicalname character varying,
    geographicalname_void character varying,
    hydroid character varying,
    hydroid_void character varying,
    relatedhydroobject integer,
    relatedhydroobject_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE t_hy_widthrange (
    ogc_fid SERIAL PRIMARY KEY,
    lower_ double precision,
    upper_ double precision,
    inspireid character varying
);

CREATE TABLE t_identifier (
    ogc_fid SERIAL PRIMARY KEY,
    namespace character varying,
    localid character varying,
    versionid character varying,
    versionid_void character varying,
    inspireid character varying
);

CREATE TABLE t_lc_countableparameter_extension (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    observationdate timestamp with time zone,
    count_ character varying,
    observationdate_void character varying,
    id_landcoverunit character varying
);

CREATE TABLE t_lc_landcoverdataset (
    ogc_fid SERIAL PRIMARY KEY,
    extent character varying,
    inspireid character varying,
    name character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_landcovernomenclature character varying
);

CREATE TABLE t_lc_landcovernomenclature (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    nomenclaturecodelist character varying,
    embeddeddescription character varying,
    embeddeddescription_void character varying,
    externaldescription_void character varying,
    id_landcoverdataset character varying,
    id_relatedparty character varying
);

CREATE TABLE t_lc_landcoverobservation (
    ogc_fid SERIAL PRIMARY KEY,
    class character varying,
    class_cl character varying,
    observationdate timestamp with time zone,
    observationdate_void character varying,
    mosaic_void character varying,
    id_landcoverobservation character varying,
    id_landcoverunit character varying,
    id_landcovernomenclature character varying
);

CREATE TABLE t_lc_landcovervalue (
    ogc_fid SERIAL PRIMARY KEY,
    class character varying,
    class_cl character varying,
    covered_percentage integer,
    covered_percentage_void character varying,
    id_landcoverobservation character varying
);

CREATE TABLE t_lc_percentageparameter_extension (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    observationdate timestamp with time zone,
    coveredpercentage character varying,
    observationdate_void character varying,
    id_landcoverunit character varying
);

CREATE TABLE t_lc_presenceparameter_extension (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    observationdate timestamp with time zone,
    present character varying,
    observationdate_void character varying,
    id_landcoverunit character varying
);

CREATE TABLE t_legislationcitation (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    shortname character varying,
    dateenteredintoforce timestamp with time zone,
    daterepealed timestamp with time zone,
    identificationnumber character varying,
    level_ character varying,
    officialdocumentnumber character varying,
    date_void character varying,
    link_void character varying,
    shortname_void character varying,
    specificreference_void character varying,
    id_legislationcitation character varying,
    id_feature character varying
);

CREATE TABLE t_lu_backgroundmapvalue (
    ogc_fid SERIAL PRIMARY KEY,
    backgroundmapdate character varying,
    backgroundmapreference character varying,
    backgroudmapuri character varying,
    backgroudmapuri_void character varying,
    id_spatialplan character varying,
    id_zoningelement character varying,
    id_supplementaryregulation character varying
);

CREATE TABLE t_lu_dimensioningindicationvalue (
    ogc_fid SERIAL PRIMARY KEY,
    indicationreference character varying,
    id_supplementaryregulation character varying,
    id_zoningelement character varying
);

CREATE TABLE t_lu_hilucspercentage (
    ogc_fid SERIAL PRIMARY KEY,
    hilucsvalue character varying,
    percentage integer,
    id_hilucspresence character varying
);

CREATE TABLE t_lu_hilucspresence (
    ogc_fid SERIAL PRIMARY KEY,
    orderedlist character varying,
    percentagelist character varying,
    id_hilucspresence character varying
);

CREATE TABLE t_lu_hilucsvalue (
    ogc_fid SERIAL PRIMARY KEY,
    hilucsvalue character varying,
    id_zoningelement character varying,
    id_existinglanduseobject character varying,
    id_existinglandusesample character varying,
    id_hilucspresence character varying
);

CREATE TABLE t_lu_landuseclassificationvalue (
    ogc_fid SERIAL PRIMARY KEY,
    landuseclassificationvalue character varying,
    landuseclassificationvalue_cl character varying,
    id_zoningelement character varying,
    id_existinglanduseobject character varying,
    id_existinglandusesample character varying,
    id_specificpresence character varying
);

CREATE TABLE t_lu_officialdocumentation (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    regulationtext character varying,
    regulationtext_void character varying,
    legislationcitation_void character varying,
    plandocument_void character varying,
    id_zoningelement character varying,
    id_spatialplan character varying,
    id_supplementaryregulation character varying
);

CREATE TABLE t_lu_ordinancevalue (
    ogc_fid SERIAL PRIMARY KEY,
    ordinancedate timestamp with time zone,
    ordinancereference character varying,
    id_spatialplan character varying
);

CREATE TABLE t_lu_specificpercentage (
    ogc_fid SERIAL PRIMARY KEY,
    specificvalue character varying,
    percentage integer,
    id_specificpresence character varying
);

CREATE TABLE t_lu_specificpresence (
    ogc_fid SERIAL PRIMARY KEY,
    orderedlist character varying,
    percentagelist character varying,
    id_specificpresence character varying
);

CREATE TABLE t_lu_specificsupplementaryregulationvalue (
    ogc_fid SERIAL PRIMARY KEY,
    specificsupplementaryregulationvalue character varying,
    specificsupplementaryregulationvalue_cl character varying,
    id_supplementaryregulation character varying
);

CREATE TABLE t_lu_supplementaryregulation_name (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    id_supplementaryregulation character varying
);

CREATE TABLE t_lu_supplementaryregulationvalue (
    ogc_fid SERIAL PRIMARY KEY,
    supplementaryregulation character varying,
    supplementaryregulation_cl character varying,
    id_supplementaryregulation character varying
);

CREATE TABLE t_md_identifier (
    ogc_fid SERIAL PRIMARY KEY,
    code character varying,
    id_citation character varying
);

CREATE TABLE t_mr_cgi_linearorientation (
    ogc_fid SERIAL PRIMARY KEY,
    directed character varying,
    plunge character varying,
    trend character varying,
    id_earthresource character varying
);

CREATE TABLE t_mr_cgi_planarorientation (
    ogc_fid SERIAL PRIMARY KEY,
    azimuth character varying,
    convention character varying,
    dip character varying,
    polarity character varying,
    id_earthresource character varying
);

CREATE TABLE t_mr_commodity (
    ogc_fid SERIAL PRIMARY KEY,
    commodity character varying,
    commodity_cl character varying,
    commodityimportance character varying,
    commodityimportance_cl character varying,
    commodityimportance_void character varying,
    commodityrank integer,
    commodityrank_void character varying,
    id_earthresource character varying,
    id_commoditymeasure character varying
);

CREATE TABLE t_mr_commoditymeasure (
    ogc_fid SERIAL PRIMARY KEY,
    commodityamount_value double precision,
    commodityamount_uom character varying,
    cutoffgrade_value double precision,
    cutoffgrade_uom character varying,
    grade_value bytea,
    grade_uom character varying,
    id_commoditymeasure character varying,
    id_oremeasure character varying
);

CREATE TABLE t_mr_earthresourcedimension (
    ogc_fid SERIAL PRIMARY KEY,
    area_void character varying,
    depth_void character varying,
    length_void character varying,
    width_void character varying,
    id_feature character varying,
    id_earthresourcedimension character varying
);

CREATE TABLE t_mr_earthresourcedimension_area (
    ogc_fid SERIAL PRIMARY KEY,
    area_value double precision,
    area_uom character varying,
    id_earthresourcedimension character varying
);

CREATE TABLE t_mr_earthresourcedimension_depth (
    ogc_fid SERIAL PRIMARY KEY,
    depth_value double precision,
    depth_uom character varying,
    id_earthresourcedimension character varying
);

CREATE TABLE t_mr_earthresourcedimension_length (
    ogc_fid SERIAL PRIMARY KEY,
    length_value double precision,
    length_uom character varying,
    id_earthresourcedimension character varying
);

CREATE TABLE t_mr_earthresourcedimension_width (
    ogc_fid SERIAL PRIMARY KEY,
    width_value double precision,
    width_uom character varying,
    id_earthresourcedimension character varying
);

CREATE TABLE t_mr_endowment (
    ogc_fid SERIAL PRIMARY KEY,
    classificationmethodused character varying,
    classificationmethodused_cl character varying,
    date_ timestamp with time zone,
    ore_value character varying,
    ore_uom character varying,
    dimension_void character varying,
    proposedextractionmethod character varying,
    proposedextractionmethod_void character varying,
    includesreserves character varying,
    includesreserves_void character varying,
    includesresources character varying,
    includesresources_void character varying,
    id_earthresources character varying,
    id_oremeasure character varying
);

CREATE TABLE t_mr_explorationactivity (
    ogc_fid SERIAL PRIMARY KEY,
    activityduration_begin timestamp with time zone,
    activityduration_end timestamp with time zone,
    activitytype character varying,
    activitytype_cl character varying,
    explorationresult_cl character varying,
    id_earthresource character varying,
    id_explorationactivity character varying
);

CREATE TABLE t_mr_explorationactivity_explorationresult (
    ogc_fid SERIAL PRIMARY KEY,
    explorationresult character varying,
    id_explorationactivity character varying
);

CREATE TABLE t_mr_mine (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    status character varying,
    status_cl character varying,
    sourcereference_void character varying,
    startdate timestamp with time zone,
    startdate_void character varying,
    enddate timestamp with time zone,
    enddate_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    relatedmine integer,
    relatedmine_void character varying
);

CREATE TABLE t_mr_minename (
    ogc_fid SERIAL PRIMARY KEY,
    ispreferred character varying,
    minename character varying,
    id_mine character varying
);

CREATE TABLE t_mr_mineraldepositmodel (
    ogc_fid SERIAL PRIMARY KEY,
    mineraldeposittype_void character varying,
    id_earthresource character varying,
    id_mineraldepositmodel character varying
);

CREATE TABLE t_mr_mineraldepositmodel_mineraldepositgroup (
    ogc_fid SERIAL PRIMARY KEY,
    mineraldepositgroup character varying,
    mineraldepositgroup_cl character varying,
    id_mineraldepositmodel character varying
);

CREATE TABLE t_mr_mineraldepositmodel_mineraldeposittype (
    ogc_fid SERIAL PRIMARY KEY,
    mineraldeposittypevalue character varying,
    mineraldeposittypevalue_cl character varying,
    id_mineraldepositmodel character varying
);

CREATE TABLE t_mr_mineraloccurrence (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    name character varying,
    name_void character varying,
    dimension character varying,
    dimension_void character varying,
    expression_void character varying,
    form_void character varying,
    linearorientation_void character varying,
    planarorientation_void character varying,
    shape_void character varying,
    sourcereference_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    type character varying,
    type_cl character varying,
    endusepotential_void character varying,
    explorationhistory_void character varying,
    resourceextraction_void character varying,
    classification_void character varying,
    oreamount_void character varying
);

CREATE TABLE t_mr_mineraloccurrence_endusepotential (
    ogc_fid SERIAL PRIMARY KEY,
    endusepotential character varying,
    endusepotential_cl character varying,
    id_mineraloccurrence character varying
);

CREATE TABLE t_mr_mineraloccurrence_expression (
    ogc_fid SERIAL PRIMARY KEY,
    expression character varying,
    id_earthresource character varying
);

CREATE TABLE t_mr_mineraloccurrence_form (
    ogc_fid SERIAL PRIMARY KEY,
    form character varying,
    id_earthresource character varying
);

CREATE TABLE t_mr_mineraloccurrence_shape (
    ogc_fid SERIAL PRIMARY KEY,
    shape character varying,
    id_earthresource character varying
);

CREATE TABLE t_mr_miningactivity (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    activityduration_begin timestamp with time zone,
    activityduration_end timestamp with time zone,
    activitytype character varying,
    activitytype_cl character varying,
    processingtype character varying,
    processingtype_cl character varying,
    oreprocessed_value character varying,
    oreprocessed_uom character varying,
    oreprocessed_void character varying,
    deposit_void character varying,
    associatedmine_void character varying,
    id_mine character varying,
    id_earthresource character varying
);

CREATE TABLE t_mr_reserve (
    ogc_fid SERIAL PRIMARY KEY,
    classificationmethodused character varying,
    classificationmethodused_cl character varying,
    date_ timestamp with time zone,
    ore_value character varying,
    ore_uom character varying,
    dimension_void character varying,
    proposedextractionmethod character varying,
    proposedextractionmethod_void character varying,
    category character varying,
    category_cl character varying,
    id_earthresources character varying,
    id_oremeasure character varying
);

CREATE TABLE t_mr_resource (
    ogc_fid SERIAL PRIMARY KEY,
    classificationmethodused character varying,
    classificationmethodused_cl character varying,
    date_ timestamp with time zone,
    ore_value character varying,
    ore_uom character varying,
    dimension_void character varying,
    proposedextractionmethod character varying,
    proposedextractionmethod_void character varying,
    category character varying,
    category_cl character varying,
    includesreserves character varying,
    includesreserves_void character varying,
    id_earthresources character varying,
    id_oremeasure character varying
);

CREATE TABLE t_nz_exposedelementclassification (
    ogc_fid SERIAL PRIMARY KEY,
    exposedelementcategory character varying,
    exposedelementcategory_cl character varying,
    specificexposedelementtype character varying,
    specificexposedelementtype_cl character varying,
    specificexposedelementtype_void character varying,
    id_vulnerabilityassessment character varying
);

CREATE TABLE t_nz_levelorintensity (
    ogc_fid SERIAL PRIMARY KEY,
    qualitativevalue character varying,
    qualitativevalue_void character varying,
    quantitativevalue_value double precision,
    quantitativevalue_uom character varying,
    quantitativevalue_void character varying,
    assessmentmethod_void character varying,
    id_levelorintensity character varying,
    id_feature character varying
);

CREATE TABLE t_nz_likelihoodofoccurrence (
    ogc_fid SERIAL PRIMARY KEY,
    qualitativelikelihood character varying,
    qualitativelikelihood_void character varying,
    quantitativelikelihood_void character varying,
    assessmentmethod_void character varying,
    id_abstracthazardarea character varying
);

CREATE TABLE t_nz_naturalhazardclassification (
    ogc_fid SERIAL PRIMARY KEY,
    hazardcategory character varying,
    hazardcategory_cl character varying,
    specifichazardtype character varying,
    specifichazardtype_cl character varying,
    specifichazardtype_void character varying,
    id_feature character varying
);

CREATE TABLE t_nz_quantitativelikelihood (
    ogc_fid SERIAL PRIMARY KEY,
    probabilityofoccurrence integer,
    probabilityofoccurrence_void character varying,
    returnperiod double precision,
    returnperiod_void character varying,
    id_feature character varying
);

CREATE TABLE t_nz_vulnerabilityassessment (
    ogc_fid SERIAL PRIMARY KEY,
    levelofvulnerability_void character varying,
    magnitudeorintensityofhazard_void character varying,
    typeofelement_void character varying,
    id_vulnerabilityassessment character varying,
    id_abstractexposedelement character varying,
    id_levelofintensity_levelofvulnerability character varying,
    id_levelofintensity_magnitudeorintensityofhazard character varying
);

CREATE TABLE t_officialjournalinformation (
    ogc_fid SERIAL PRIMARY KEY,
    isbn character varying,
    issn character varying,
    linktojournal character varying,
    officialjournalidentification character varying,
    id_legislationcitation character varying
);

CREATE TABLE t_oi_orthoimagecoverage_ex_extent (
    ogc_fid SERIAL PRIMARY KEY,
    domainextent character varying,
    id_ortoimagecoverage character varying
);

CREATE TABLE t_om_categoryconstraint (
    ogc_fid SERIAL PRIMARY KEY,
    constrainedproperty character varying,
    constrainedproperty_cl character varying,
    label character varying,
    comparison character varying,
    comparison_cl character varying,
    id_categoryconstraint character varying,
    id_observableproperty character varying
);

CREATE TABLE t_om_categoryconstraint_value (
    ogc_fid SERIAL PRIMARY KEY,
    value_ character varying,
    id_categoryconstraint character varying
);

CREATE TABLE t_om_compositeobservableproperty (
    ogc_fid SERIAL PRIMARY KEY,
    count_ integer,
    id_compositeobservableproperty character varying
);

CREATE TABLE t_om_namedvalue (
    ogc_fid SERIAL PRIMARY KEY,
    parameter_name character varying,
    parameter_value character varying,
    id_observation character varying,
    id_samplingfeature character varying
);

CREATE TABLE t_om_observableproperty (
    ogc_fid SERIAL PRIMARY KEY,
    basephenomenontype character varying,
    basephenomenontype_cl character varying,
    uom character varying,
    id_observableproperty character varying
);

CREATE TABLE t_om_observableproperty_label (
    ogc_fid SERIAL PRIMARY KEY,
    label character varying,
    id_observableproperty character varying
);

CREATE TABLE t_om_observation (
    ogc_fid SERIAL PRIMARY KEY,
    phenomenontime timestamp with time zone,
    resulttime timestamp with time zone,
    validtime_begin timestamp with time zone,
    validtime_end timestamp with time zone,
    id_feature character varying,
    id_observation character varying,
    id_observationset character varying
);

CREATE TABLE t_om_observationset (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    extent character varying,
    id_feature character varying
);

CREATE TABLE t_om_otherconstraint (
    ogc_fid SERIAL PRIMARY KEY,
    constrainedproperty character varying,
    constrainedproperty_cl character varying,
    label character varying,
    description character varying,
    id_observableproperty character varying
);

CREATE TABLE t_om_process (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    inspireid_void character varying,
    name character varying,
    name_void character varying,
    type character varying,
    type_void character varying,
    documentation_void character varying,
    processparameter_void character varying,
    responsibleparty_void character varying,
    id_feature character varying
);

CREATE TABLE t_om_processparameter (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    name_cl character varying,
    description character varying,
    id_process character varying
);

CREATE TABLE t_om_rangebounds (
    ogc_fid SERIAL PRIMARY KEY,
    startcomparison character varying,
    startcomparison_cl character varying,
    rangestart double precision,
    endcomparison character varying,
    endcomparison_cl character varying,
    rangeend double precision,
    id_rangeconstraint character varying
);

CREATE TABLE t_om_rangeconstraint (
    ogc_fid SERIAL PRIMARY KEY,
    constrainedproperty character varying,
    constrainedproperty_cl character varying,
    label character varying,
    uom character varying,
    id_rangeconstraint character varying,
    id_observableproperty character varying
);

CREATE TABLE t_om_scalarconstraint (
    ogc_fid SERIAL PRIMARY KEY,
    constrainedproperty character varying,
    constrainedproperty_cl character varying,
    label character varying,
    comparison character varying,
    comparison_cl character varying,
    uom character varying,
    id_scalarconstraint character varying,
    id_observableproperty character varying
);

CREATE TABLE t_om_scalarconstraint_value (
    ogc_fid SERIAL PRIMARY KEY,
    value_ double precision,
    id_scalarconstraint character varying
);

CREATE TABLE t_om_statisticalmeasure (
    ogc_fid SERIAL PRIMARY KEY,
    label character varying,
    statisticalfunction character varying,
    statisticalfunction_cl character varying,
    aggregationtimeperiod timestamp with time zone,
    aggregationlength integer,
    aggregationarea double precision,
    aggregationvolume double precision,
    otheraggregation character varying,
    derivedfrom character varying,
    id_observableproperty character varying
);

CREATE TABLE t_partyrolevalue (
    ogc_fid SERIAL PRIMARY KEY,
    relatedpartyrolevalue character varying,
    id_relatedparty character varying
);

CREATE TABLE t_pd_classification (
    ogc_fid SERIAL PRIMARY KEY,
    type character varying,
    type_cl character varying,
    id_statisticaldistribution character varying
);

CREATE TABLE t_pd_classificationitem (
    ogc_fid SERIAL PRIMARY KEY,
    type1 character varying,
    type1_cl character varying,
    type2 character varying,
    type2_cl character varying,
    type3 character varying,
    type3_cl character varying,
    type4 character varying,
    type4_cl character varying,
    type5 character varying,
    type5_cl character varying,
    id_classification character varying,
    id_dimensions character varying
);

CREATE TABLE t_pd_dimensions (
    ogc_fid SERIAL PRIMARY KEY,
    id_statisticalvalue character varying,
    id_statisticalunit character varying
);

CREATE TABLE t_pd_statisticalvalue (
    ogc_fid SERIAL PRIMARY KEY,
    value_ double precision,
    specialvalue character varying,
    specialvalue_cl character varying,
    conventionallylocatedproportion double precision,
    approximatelylocatedpopulationproportion double precision,
    comment character varying,
    flags character varying,
    status character varying,
    periodofmeasurement_begin timestamp with time zone,
    periodofmeasurement_end timestamp with time zone,
    periodofmeasurement_void character varying,
    status_cl character varying,
    id_statisticaldistribution character varying,
    id_statisticalvalue character varying
);

CREATE TABLE t_pf_description (
    ogc_fid SERIAL PRIMARY KEY,
    description character varying,
    id_feature character varying
);

CREATE TABLE t_pf_name (
    ogc_fid SERIAL PRIMARY KEY,
    name character varying,
    id_feature character varying
);

CREATE TABLE t_pf_statustype (
    ogc_fid SERIAL PRIMARY KEY,
    statustype character varying,
    statustype_cl character varying,
    description character varying,
    description_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_feature character varying
);

CREATE TABLE t_ps_designationtype (
    ogc_fid SERIAL PRIMARY KEY,
    designationscheme integer,
    designationscheme_cl character varying,
    designation character varying,
    designation_cl character varying,
    percentageunderdesignation double precision,
    id_protectedsite character varying
);

CREATE TABLE t_ps_siteprotectionclassification (
    ogc_fid SERIAL PRIMARY KEY,
    siteprotectionclassification character varying,
    siteprotectionclassification_cl character varying,
    id_protectedsite character varying
);

CREATE TABLE t_relatedparty (
    ogc_fid SERIAL PRIMARY KEY,
    individualname character varying,
    individualname_void character varying,
    organisationname character varying,
    organisationname_void character varying,
    positionname character varying,
    positionname_void character varying,
    role character varying,
    role_void character varying,
    id_relatedparty character varying,
    id_feature character varying
);

CREATE TABLE t_reporttolegalact (
    ogc_fid SERIAL PRIMARY KEY,
    reportdate character varying,
    reportdate_void character varying,
    reportedenvelope character varying,
    reportedenvelope_void character varying,
    observationrequired character varying,
    observationrequired_void character varying,
    observingcapabilityrequired character varying,
    observingcapabilityrequired_void character varying,
    description character varying,
    description_void character varying,
    id_reporttolegalact character varying,
    id_feature character varying
);

CREATE TABLE t_sd_distributioninfotype (
    ogc_fid SERIAL PRIMARY KEY,
    occurrencecategory character varying,
    occurrencecategory_cl character varying,
    collectedfrom timestamp with time zone,
    collectedfrom_void character varying,
    collectedto timestamp with time zone,
    collectedto_void character varying,
    populationsize_void character varying,
    populationtype character varying,
    populationtype_cl character varying,
    populationtype_void character varying,
    residencystatus character varying,
    residencystatus_cl character varying,
    residencystatus_void character varying,
    sensitiveinfo character varying,
    sensitiveinfo_void character varying,
    id_speciesdistributionunit character varying,
    id_distributioninfotype character varying
);

CREATE TABLE t_sd_populationsizetype (
    ogc_fid SERIAL PRIMARY KEY,
    countingmethod character varying,
    countingmethod_cl character varying,
    countingunit character varying,
    countingunit_cl character varying,
    id_populationsizetype character varying,
    id_distributioninfotype character varying
);

CREATE TABLE t_sd_rangetype (
    ogc_fid SERIAL PRIMARY KEY,
    lowerbound integer,
    upperbound integer,
    id_populationsizetype character varying
);

CREATE TABLE t_sd_speciesnametype (
    ogc_fid SERIAL PRIMARY KEY,
    referencespeciesid character varying,
    referencespeciesid_cl character varying,
    referencespeciesscheme character varying,
    referencespeciesscheme_cl character varying,
    localspeciesid character varying,
    localspeciesid_cl character varying,
    localspeciesid_void character varying,
    localspeciesname character varying,
    localspeciesname_void character varying,
    localspeciesscheme character varying,
    localspeciesscheme_void character varying,
    qualifier character varying,
    qualifier_cl character varying,
    qualifier_void character varying,
    referencespeciesname character varying,
    referencespeciesname_void character varying,
    id_speciesdistributionunit character varying
);

CREATE TABLE t_so_derivedsoilprofile (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    localidentifier character varying,
    localidentifier_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    wrbsoilname_void character varying,
    othersoilname_void character varying,
    isderivedfrom_void character varying,
    id_soilbody character varying
);

CREATE TABLE t_so_eventenvironmentvalue (
    ogc_fid SERIAL PRIMARY KEY,
    layergenesisenvironment character varying,
    layergenesisenvironment_cl character varying,
    id_soillayer character varying
);

CREATE TABLE t_so_eventprocessvalue (
    ogc_fid SERIAL PRIMARY KEY,
    layergenesisprocess character varying,
    layergenesisprocess_cl character varying,
    id_soillayer character varying
);

CREATE TABLE t_so_faohorizonnotationtype (
    ogc_fid SERIAL PRIMARY KEY,
    faohorizondiscontinuity integer,
    faohorizonmaster character varying,
    faohorizonmaster_cl character varying,
    faoprime character varying,
    faoprime_cl character varying,
    faohorizonvertical integer,
    isoriginalclassification character varying,
    id_soilhorizon character varying
);

CREATE TABLE t_so_faohorizonsubordinatevalue (
    ogc_fid SERIAL PRIMARY KEY,
    faohorizonsubordinatevalue character varying,
    faohorizonsubordinatevalue_cl character varying,
    id_faohorizonnotationtype character varying
);

CREATE TABLE t_so_lithologyvalue (
    ogc_fid SERIAL PRIMARY KEY,
    layerrocktype character varying,
    layerrocktype_cl character varying,
    id_soillayer character varying
);

CREATE TABLE t_so_observedsoilprofile (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    localidentifier character varying,
    localidentifier_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    wrbsoilname_void character varying,
    othersoilname_void character varying,
    id_soilderivedobject character varying,
    id_derivedsoilprofile character varying
);

CREATE TABLE t_so_otherhorizonnotationtype (
    ogc_fid SERIAL PRIMARY KEY,
    horizonnotation character varying,
    isoriginalclassification character varying,
    id_soilhorizon character varying
);

CREATE TABLE t_so_othersoilnametype (
    ogc_fid SERIAL PRIMARY KEY,
    soilname character varying,
    soilname_cl character varying,
    isoriginalclassification character varying,
    id_soilprofile character varying
);

CREATE TABLE t_so_particlesizefractiontype (
    ogc_fid SERIAL PRIMARY KEY,
    fractioncontent integer,
    id_profileelement character varying
);

CREATE TABLE t_so_rangetype (
    ogc_fid SERIAL PRIMARY KEY,
    uppervalue double precision,
    lowervalue double precision,
    uom character varying,
    id_feature character varying
);

CREATE TABLE t_so_soilhorizon (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    particlesizefraction_void character varying,
    faohorizonnotation_void character varying,
    otherhorizonnotation_void character varying,
    id_soilprofile character varying
);

CREATE TABLE t_so_soillayer (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    layertype character varying,
    layertype_cl character varying,
    layergenesisprocess character varying,
    layergenesisprocess_void character varying,
    layergenesisenvironment character varying,
    layergenesisenvironment_void character varying,
    layergenesisprocessstate character varying,
    layergenesisprocessstate_void character varying,
    particlesizefraction_void character varying,
    layerrocktype_void character varying,
    id_soilprofile character varying
);

CREATE TABLE t_sr_parametervaluepair (
    ogc_fid SERIAL PRIMARY KEY,
    value_ character varying,
    validtime character varying,
    validtime_void character varying,
    id_abstractobservableproperty character varying,
    id_seaarea character varying
);

CREATE TABLE t_su_evolution (
    ogc_fid SERIAL PRIMARY KEY,
    date_ timestamp with time zone,
    evolutiontype character varying,
    evolutiontype_cl character varying,
    areavariation double precision,
    areavariation_void character varying,
    populationvariation integer,
    populationvariation_void character varying,
    initialunitversions character varying,
    initialunitversions_void character varying,
    finalunitversions character varying,
    finalunitversions_void character varying,
    id_evolution character varying
);

CREATE TABLE t_su_vectorstatisticalunit (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    thematicid character varying,
    country character varying,
    geographicalname character varying,
    validityperiod_begin timestamp with time zone,
    validityperiod_end timestamp with time zone,
    referenceperiod_begin timestamp with time zone,
    referenceperiod_end timestamp with time zone,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE t_thematicidentifier (
    ogc_fid SERIAL PRIMARY KEY,
    identifier character varying,
    identifierscheme character varying,
    id_feature character varying
);

CREATE TABLE t_tn_a_aerodromecategory (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    aerodromecategory character varying,
    aerodromecategory_cl character varying,
    id_aerodromenode character varying,
    id_aerodromearea character varying
);

CREATE TABLE t_tn_a_aerodrometype (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    aerodrometype character varying,
    aerodrometype_cl character varying,
    id_aerodromenode character varying,
    id_aerodromearea character varying
);

CREATE TABLE t_tn_a_conditionofairfacility (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    id_aerodromenode character varying,
    id_aerodromearea character varying,
    id_runwayarea character varying
);

CREATE TABLE t_tn_a_elementlength (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    length_value double precision,
    length_uom character varying,
    id_runwayarea character varying,
    id_taxiwayarea character varying,
    id_touchdownliftoff character varying
);

CREATE TABLE t_tn_a_elementwidth (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    width_value double precision,
    width_uom character varying,
    id_runwayarea character varying,
    id_taxiwayarea character varying,
    id_touchdownliftoff character varying
);

CREATE TABLE t_tn_a_fieldelevation (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    altitude_value double precision,
    altitude_uom character varying,
    id_aerodromenode character varying,
    id_aerodromearea character varying
);

CREATE TABLE t_tn_a_loweraltitudelimit (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    altitude_value double precision,
    altitude_uom character varying,
    id_airroutelink character varying,
    id_airspacearea character varying
);

CREATE TABLE t_tn_a_surfacecomposition (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    surfacecomposition character varying,
    surfacecomposition_cl character varying,
    id_runwayarea character varying,
    id_taxiwayarea character varying,
    id_apronarea character varying,
    id_touchdowhliftoff character varying
);

CREATE TABLE t_tn_a_upperaltitudelimit (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    altitude_value double precision,
    altitude_uom character varying,
    id_airroutelink character varying,
    id_airspacearea character varying
);

CREATE TABLE t_tn_a_userestriction (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    restriction character varying,
    restriction_cl character varying,
    id_aerodromearea character varying,
    id_navaid character varying,
    id_designatedpoint character varying,
    id_runwaycentrelinepoint character varying,
    id_touchdownliftoff character varying,
    id_aerodromenode character varying,
    id_airroute character varying,
    id_airroutelink character varying,
    id_standardinstrumentdeparture character varying,
    id_instrumentapproachprocedure character varying,
    id_standardinstrumentarrival character varying
);

CREATE TABLE t_tn_c_accessrestriction (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    restriction character varying,
    restriction_cl character varying,
    id_transportlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_c_conditionoffacility (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    id_transportlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_c_crossreference (
    ogc_fid SERIAL PRIMARY KEY,
    id_crossreference character varying
);

CREATE TABLE t_tn_c_directedlink (
    ogc_fid SERIAL PRIMARY KEY,
    direction character varying,
    id_link character varying,
    id_linksequence character varying
);

CREATE TABLE t_tn_c_gradeseparatedcrossing (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    id_networkconnection character varying,
    id_network character varying
);

CREATE TABLE t_tn_c_maintenanceauthority (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_transportlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_c_networkconnection (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    connectiontype character varying,
    connectiontype_cl character varying,
    id_roadnode character varying,
    id_portnode character varying,
    id_aerodromenode character varying
);

CREATE TABLE t_tn_c_ownerauthority (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_transportlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_c_restrictionforvehicles (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    restricitontype character varying,
    restricitontype_cl character varying,
    measure_value double precision,
    measure_uom character varying,
    id_transportlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_c_simplelinearreference (
    ogc_fid SERIAL PRIMARY KEY,
    applicabledirection character varying,
    applicabledirection_cl character varying,
    fromposition double precision,
    fromposition_uom character varying,
    toposition double precision,
    toposition_uom character varying,
    "offset" double precision,
    offset_uom character varying,
    offset_void character varying,
    id_networkproperty character varying
);

CREATE TABLE t_tn_c_simplepointreference (
    ogc_fid SERIAL PRIMARY KEY,
    applicabledirection character varying,
    applicabledirection_cl character varying,
    atposition double precision,
    atposition_uom character varying,
    "offset" double precision,
    offset_uom character varying,
    offset_void character varying,
    id_networkproperty character varying
);

CREATE TABLE t_tn_c_trafficflowdirection (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    direction character varying,
    direction_cl character varying,
    id_transportlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_c_transportnetwork (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    typeoftransport character varying,
    typeoftransport_cl character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE t_tn_c_verticalposition (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    id_transportlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_r_formofway (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    formofway character varying,
    formofway_cl character varying,
    id_roadlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_r_functionalroadclass (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    funtionalclass character varying,
    funtionalclass_cl character varying,
    id_roadlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_r_numberoflanes (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    direction character varying,
    direction_cl character varying,
    direction_void character varying,
    numberoflanes integer,
    numberoflanes_void character varying,
    minmaxnumberoflanes character varying,
    minmaxnumberoflanes_void character varying,
    minmaxnumberoflanes_cl character varying,
    id_roadlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_r_roadname (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    name character varying,
    id_geographicalname_namedplace character varying,
    id_roadlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_r_roadservicetype (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    availablefacility character varying,
    availablefacility_cl character varying,
    type character varying,
    type_cl character varying,
    id_roadservicearea character varying,
    id_transportlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying
);

CREATE TABLE t_tn_r_roadsurfacecategory (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    surfacecategory character varying,
    surfacecategory_cl character varying,
    id_roadlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_r_roadwidth (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    measureroadpart character varying,
    measureroadpart_cl character varying,
    measureroadpart_void character varying,
    width_value double precision,
    width_uom character varying,
    id_roadlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_r_speedlimit (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    speedlimitminmaxtype character varying,
    speedlimitvalue character varying,
    areacondition character varying,
    areacondition_cl character varying,
    areacondition_void character varying,
    direction character varying,
    direction_cl character varying,
    direction_void character varying,
    laneextension integer,
    laneextension_void character varying,
    speedlimitsource character varying,
    speedlimitsource_cl character varying,
    speedlimitsource_void character varying,
    startlane integer,
    startlane_void character varying,
    validityperiod character varying,
    validityperiod_void character varying,
    vehicletype character varying,
    vehicletype_cl character varying,
    vehicletype_void character varying,
    weathercondition character varying,
    weathercondition_cl character varying,
    weathercondition_void character varying,
    id_roadlink character varying,
    id_transportnode character varying,
    id_markerpost character varying,
    id_transportlinksequence character varying,
    id_transportlinkset character varying,
    id_transportarea character varying
);

CREATE TABLE t_tn_w_cemtclass (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    cemclass character varying,
    cemtclass_cl character varying,
    id_inlandwater character varying
);

CREATE TABLE t_tn_w_conditionofwaterfacility (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    id_portnode character varying,
    id_portarea character varying
);

CREATE TABLE t_tn_w_ferryuse (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_ferrycrossing character varying
);

CREATE TABLE t_tn_w_ferryusevalue (
    ogc_fid SERIAL PRIMARY KEY,
    ferryuse character varying,
    ferryuse_cl character varying,
    id_ferryuse character varying
);

CREATE TABLE t_tn_w_restrictionforwatervehicles (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    measure double precision,
    restrictiontype character varying,
    restrictiontype_cl character varying,
    id_waterwaylink character varying,
    id_waterwaynode character varying
);

CREATE TABLE t_tn_w_watertrafficflowdirection (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    networkref_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    direction character varying,
    direction_cl character varying,
    id_waterwaylink character varying,
    id_waterwaylinksequence character varying
);

CREATE TABLE t_us_environmentalmanagementfacilitytypevalue (
    ogc_fid SERIAL PRIMARY KEY,
    type character varying,
    type_cl character varying,
    id_environmentalmanagementfacility character varying
);

CREATE TABLE t_us_governmentalservice (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    servicetype character varying,
    servicetype_cl character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    areaofresponsibility_void character varying,
    pointofcontact_void character varying,
    id_feature character varying
);

CREATE TABLE t_us_noteextension (
    ogc_fid SERIAL PRIMARY KEY,
    note_ character varying,
    note_void character varying,
    id_governmentalserviceextension character varying
);

CREATE TABLE t_us_oilgaschemicalsproducttypevalue (
    ogc_fid SERIAL PRIMARY KEY,
    oilgaschemicalsproducttype character varying,
    oilgaschemicalsproducttype_cl character varying,
    id_oilgaschemicalspipe character varying
);

CREATE TABLE t_us_servicelevelvalueextension (
    ogc_fid SERIAL PRIMARY KEY,
    servicelevel character varying,
    servicelevel_void character varying,
    id_governmentalserviceextension character varying
);

CREATE TABLE t_us_utilitynetwork (
    ogc_fid SERIAL PRIMARY KEY,
    inspireid character varying,
    geographicalname character varying,
    utilitynetworktype character varying,
    utilitynetworktype_cl character varying,
    disclaimer character varying,
    disclaimer_void character varying,
    utilityfacilityreference_void character varying,
    networks integer,
    networks_void character varying,
    id_feature character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnaaerodromearea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnaaerodromearea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnaaerodromenode_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    designatoriata character varying,
    designatoriata_void character varying,
    locationindicatoricao character varying,
    locationindicatoricao_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnaaerodromenode_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    designatoriata character varying,
    designatoriata_void character varying,
    locationindicatoricao character varying,
    locationindicatoricao_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnaairlinksequence_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_waterway character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_linkset character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnaairlinksequence_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_waterway character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_linkset character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnaairroute_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    airroutetype character varying,
    airroutetype_cl character varying,
    airroutetype_void character varying,
    designator character varying,
    designator_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnaairroute_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    airroutetype character varying,
    airroutetype_cl character varying,
    airroutetype_void character varying,
    designator character varying,
    designator_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnaairroutelink_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    airroutelinkclass character varying,
    airroutelinkclass_cl character varying,
    airroutelinkclass_void character varying,
    endnode character varying,
    startnode character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE tnaairroutelink_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    airroutelinkclass character varying,
    airroutelinkclass_cl character varying,
    airroutelinkclass_void character varying,
    endnode character varying,
    startnode character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE tnaairspacearea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    airspaceareatype character varying,
    airspaceareatype_cl character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnaairspacearea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    airspaceareatype character varying,
    airspaceareatype_cl character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnaapronarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnaapronarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnadesignatedpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    designator character varying,
    designator_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnadesignatedpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    designator character varying,
    designator_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnainstrumentapproachprocedure_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    endnode character varying,
    startnode character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE tnainstrumentapproachprocedure_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    endnode character varying,
    startnode character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE tnanavaid_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    designator character varying,
    designator_void character varying,
    navaidtype character varying,
    navaidtype_cl character varying,
    navaidtype_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnanavaid_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokestart_void character varying,
    spokeend_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    designator character varying,
    designator_void character varying,
    navaidtype character varying,
    navaidtype_cl character varying,
    navaidtype_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnarunwayarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    designator character varying,
    designator_void character varying,
    runwaytype character varying,
    runwaytype_cl character varying,
    runwaytype_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnarunwayarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    designator character varying,
    designator_void character varying,
    runwaytype character varying,
    runwaytype_cl character varying,
    runwaytype_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnarunwaycentrelinepoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    pointrole character varying,
    pointrole_cl character varying,
    pointrole_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnarunwaycentrelinepoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    pointrole character varying,
    pointrole_cl character varying,
    pointrole_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnastandardinstrumentarrival_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    designator character varying,
    designator_void character varying,
    endnode character varying,
    startnode character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE tnastandardinstrumentarrival_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    designator character varying,
    designator_void character varying,
    endnode character varying,
    startnode character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE tnastandardinstrumentdeparture_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    designator character varying,
    designator_void character varying,
    endnode character varying,
    startnode character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE tnastandardinstrumentdeparture_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    designator character varying,
    designator_void character varying,
    endnode character varying,
    startnode character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    shape_length double precision
);

CREATE TABLE tnataxiwayarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    designator character varying,
    designator_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnataxiwayarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    designator character varying,
    designator_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnatouchdownliftoff_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    designator character varying,
    designator_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnatouchdownliftoff_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend_void character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    significantpoint character varying,
    designator character varying,
    designator_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tncmarkerpost_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    location double precision,
    route_void character varying,
    id_namedplace_geographicalname character varying,
    id_transportlinkset character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    innetwork_void character varying
);

CREATE TABLE tncmarkerpost_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    location double precision,
    route_void character varying,
    id_namedplace_geographicalname character varying,
    id_transportlinkset character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    innetwork_void character varying
);

CREATE TABLE tnctransportarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_namedplace_geographicalname character varying,
    innetwork_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnctransportarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_namedplace_geographicalname character varying,
    innetwork_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnctransportlink_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    ficititious character varying,
    endnode character varying,
    startnode character varying,
    id_linkset character varying,
    id_namedplace_geographicalname character varying,
    id_networkconnection character varying,
    id_gradeseparatedcrossing character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    innetwork_void character varying,
    shape_length double precision
);

CREATE TABLE tnctransportlink_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    ficititious character varying,
    endnode character varying,
    startnode character varying,
    id_linkset character varying,
    id_namedplace_geographicalname character varying,
    id_networkconnection character varying,
    id_gradeseparatedcrossing character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    innetwork_void character varying,
    shape_length double precision
);

CREATE TABLE tnctransportlinksequence_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_namedplace_geographicalname character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_linkset character varying,
    innetwork_void character varying,
    shape_length double precision
);

CREATE TABLE tnctransportlinksequence_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_namedplace_geographicalname character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_linkset character varying,
    innetwork_void character varying,
    shape_length double precision
);

CREATE TABLE tnctransportlinkset_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_namedplace_geographicalname character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    innetwork_void character varying,
    shape_length double precision
);

CREATE TABLE tnctransportlinkset_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_namedplace_geographicalname character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    innetwork_void character varying,
    shape_length double precision
);

CREATE TABLE tnctransportnode_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_namedplace_geographicalname character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    spokestart character varying,
    spokestart_void character varying,
    spokeend character varying,
    spokeend_void character varying,
    innetwork_void character varying
);

CREATE TABLE tnctransportnode_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_namedplace_geographicalname character varying,
    id_networkconnection character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    spokestart character varying,
    spokestart_void character varying,
    spokeend character varying,
    spokeend_void character varying,
    innetwork_void character varying
);

CREATE TABLE tnrroad_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    localroadcode character varying,
    localroadcode_void character varying,
    nationalroadcode character varying,
    nationalroadcode_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnrroad_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    localroadcode character varying,
    localroadcode_void character varying,
    nationalroadcode character varying,
    nationalroadcode_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnrroadarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnrroadarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnrroadlink_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    ficititious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    endnode character varying,
    startnode character varying,
    id_road character varying,
    id_thoroughfarename character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_linkset character varying,
    id_gradeseparatedcrossing character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnrroadlink_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    ficititious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    startnode character varying,
    endnode character varying,
    id_road character varying,
    id_thoroughfarename character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_gradeseparatedcrossing character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnrroadlinksequence_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_road character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnrroadlinksequence_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_road character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnrroadnode_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend character varying,
    spokeend_void character varying,
    spokestart character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    formofroadnode character varying,
    formofroadnode_cl character varying,
    formofroadnode_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnrroadnode_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend character varying,
    spokeend_void character varying,
    spokestart character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    formofroadnode character varying,
    formofroadnode_cl character varying,
    formofroadnode_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnrroadservicearea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnrroadservicearea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnrvehicletrafficarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnrvehicletrafficarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwbeacon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_trafficseparationscheme character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnwbeacon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_trafficseparationscheme character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnwbuoy_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_trafficseparationscheme character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnwbuoy_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_trafficseparationscheme character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnwfairwayarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwfairwayarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwferrycrossing_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwferrycrossing_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwinlandwaterway_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwinlandwaterway_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwmarinewaterway_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    deepwaterroute character varying,
    deepwaterroute_void character varying,
    id_trafficseparationscheme character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwmarinewaterway_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    post_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    deepwaterroute character varying,
    deepwaterroute_void character varying,
    id_trafficseparationscheme character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwportarea_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwportarea_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwportnode_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend character varying,
    spokeend_void character varying,
    spokestart character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validto timestamp with time zone,
    validfrom_void character varying,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnwportnode_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend character varying,
    spokeend_void character varying,
    spokestart character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnwtrafficseparationschemecrossing_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwtrafficseparationschemecrossing_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwtrafficseparationschemelane_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwtrafficseparationschemelane_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwtrafficseparationschemeroundabout_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwtrafficseparationschemeroundabout_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwtrafficseparationschemeseparator_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwtrafficseparationschemeseparator_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE tnwwaterwaylink_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    endnode character varying,
    startnode character varying,
    id_waterway character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_gradeseparatedcrossing character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwwaterwaylink_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    fictitious character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    endnode character varying,
    startnode character varying,
    id_waterway character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_gradeseparatedcrossing character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwwaterwaylinksequence_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_waterway character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_linkset character varying,
    id_namedplace_geographicalname character varying,
    shape_length double precision
);

CREATE TABLE tnwwaterwaylinksequence_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    id_waterway character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying,
    id_linkset character varying,
    shape_length double precision
);

CREATE TABLE tnwwaterwaynode_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend character varying,
    spokeend_void character varying,
    spokestart character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    formofwaterwaynode character varying,
    formofwaterwaynode_cl character varying,
    formofwaterwaynode_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE tnwwaterwaynode_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    geographicalname_void character varying,
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    innetwork_void character varying,
    spokeend character varying,
    spokeend_void character varying,
    spokestart character varying,
    spokestart_void character varying,
    validfrom timestamp with time zone,
    validto timestamp with time zone,
    validfrom_void character varying,
    validto_void character varying,
    formofwaterwaynode character varying,
    formofwaterwaynode_cl character varying,
    formofwaterwaynode_void character varying,
    id_crossreference character varying,
    id_transportnetwork character varying,
    id_networkconnection character varying,
    id_namedplace_geographicalname character varying
);

CREATE TABLE usappurtenance_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    appurtenancetype character varying,
    appurtenancetype_cl character varying,
    appurtenancetype_void character varying,
    specificappurtenancetype character varying,
    specificappurtenancetype_cl character varying,
    specificappurtenancetype_void character varying,
    id_feature character varying
);

CREATE TABLE usappurtenance_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    appurtenancetype character varying,
    appurtenancetype_cl character varying,
    appurtenancetype_void character varying,
    specificappurtenancetype character varying,
    specificappurtenancetype_cl character varying,
    specificappurtenancetype_void character varying,
    id_feature character varying
);

CREATE TABLE usareaofresponsibility_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_governmentalservice character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE usareaofresponsibility_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_governmentalservice character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE uscabinet_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying
);

CREATE TABLE uscabinet_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying
);

CREATE TABLE usduct_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    ductwidth double precision,
    ductwidth_void character varying,
    ducts integer,
    ducts_void character varying,
    shape_length double precision
);

CREATE TABLE usduct_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_void character varying,
    warningtype_cl character varying,
    ductwidth double precision,
    ductwidth_void character varying,
    ducts integer,
    ducts_void character varying,
    shape_length double precision
);

CREATE TABLE uselectricitycable_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    operatingvoltage_value character varying,
    operatingvoltage_uom character varying,
    operatingvoltage_void character varying,
    nominalvoltage_value character varying,
    nominalvoltage_uom character varying,
    nominalvoltage_void character varying,
    id_pipe character varying,
    id_duct character varying,
    shape_length double precision
);

CREATE TABLE uselectricitycable_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    operatingvoltage_value character varying,
    operatingvoltage_uom character varying,
    operatingvoltage_void character varying,
    nominalvoltage_value character varying,
    nominalvoltage_uom character varying,
    nominalvoltage_void character varying,
    id_pipe character varying,
    id_duct character varying,
    shape_length double precision
);

CREATE TABLE usenvironmentalmanagementfacilityline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    servicehours character varying,
    servicehours_void character varying,
    status character varying,
    status_cl character varying,
    status_void character varying,
    type_void character varying,
    facilitydescription_void character varying,
    physicalcapacity_void character varying,
    permission_void character varying,
    parentfacility integer,
    parentfacility_void character varying,
    shape_length double precision
);

CREATE TABLE usenvironmentalmanagementfacilityline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    servicehours character varying,
    servicehours_void character varying,
    status character varying,
    status_cl character varying,
    status_void character varying,
    type_void character varying,
    facilitydescription_void character varying,
    physicalcapacity_void character varying,
    permission_void character varying,
    parentfacility integer,
    parentfacility_void character varying,
    shape_length double precision
);

CREATE TABLE usenvironmentalmanagementfacilitypoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    servicehours character varying,
    servicehours_void character varying,
    status character varying,
    status_cl character varying,
    status_void character varying,
    type_void character varying,
    facilitydescription_void character varying,
    physicalcapacity_void character varying,
    permission_void character varying,
    parentfacility integer,
    parentfacility_void character varying
);

CREATE TABLE usenvironmentalmanagementfacilitypoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    servicehours character varying,
    servicehours_void character varying,
    status character varying,
    status_cl character varying,
    status_void character varying,
    type_void character varying,
    facilitydescription_void character varying,
    physicalcapacity_void character varying,
    permission_void character varying,
    parentfacility integer,
    parentfacility_void character varying
);

CREATE TABLE usenvironmentalmanagementfacilitypolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    inspireid character varying,
    name character varying,
    name_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    servicehours character varying,
    servicehours_void character varying,
    status character varying,
    status_cl character varying,
    status_void character varying,
    type_void character varying,
    facilitydescription_void character varying,
    physicalcapacity_void character varying,
    permission_void character varying,
    parentfacility integer,
    parentfacility_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE usenvironmentalmanagementfacilitypolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    inspireid character varying,
    name character varying,
    name_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    servicehours character varying,
    servicehours_void character varying,
    status character varying,
    status_cl character varying,
    status_void character varying,
    type_void character varying,
    facilitydescription_void character varying,
    physicalcapacity_void character varying,
    permission_void character varying,
    parentfacility integer,
    parentfacility_void character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE usmanhole_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying
);

CREATE TABLE usmanhole_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying
);

CREATE TABLE usoilgaschemicalspipe_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    pipediameter_value character varying,
    pipediameter_unitofmeaure character varying,
    pipediameter_void character varying,
    pressure_value character varying,
    pressure_unitofmeaure character varying,
    pressure_void character varying,
    id_duct character varying,
    id_pipe character varying,
    shape_length double precision
);

CREATE TABLE usoilgaschemicalspipe_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    pipediameter_value character varying,
    pipediameter_unitofmeaure character varying,
    pipediameter_void character varying,
    pressure_value character varying,
    pressure_unitofmeaure character varying,
    pressure_void character varying,
    id_pipe character varying,
    id_duct character varying,
    shape_length double precision
);

CREATE TABLE uspipe_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    pipediameter_value character varying,
    pipediameter_uom character varying,
    pipediameter_void character varying,
    pressure_value character varying,
    pressure_uom character varying,
    pressure_void character varying,
    pipes integer,
    pipes_void character varying,
    id_feature character varying,
    shape_length double precision
);

CREATE TABLE uspipe_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    pipediameter_value character varying,
    pipediameter_uom character varying,
    pipediameter_void character varying,
    pressure_value character varying,
    pressure_uom character varying,
    pressure_void character varying,
    pipes integer,
    pipes_void character varying,
    id_feature character varying,
    shape_length double precision
);

CREATE TABLE uspole_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    poleheight double precision,
    poleheight_void character varying
);

CREATE TABLE uspole_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    poleheight double precision,
    poleheight_void character varying
);

CREATE TABLE usservicelocationline_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    id_governmentalservice character varying,
    shape_length double precision
);

CREATE TABLE usservicelocationline_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    id_governmentalservice character varying,
    shape_length double precision
);

CREATE TABLE usservicelocationpoint_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    id_governmentalservice character varying
);

CREATE TABLE usservicelocationpoint_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    id_governmentalservice character varying
);

CREATE TABLE usservicelocationpolygon_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900915),
    id_governmentalservice character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE usservicelocationpolygon_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiPolygon,900914),
    id_governmentalservice character varying,
    shape_length double precision,
    shape_area double precision
);

CREATE TABLE ussewerpipe_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    pipediameter_value character varying,
    pipediameter_unitofmeaure character varying,
    pipediameter_void character varying,
    pressure_value character varying,
    pressure_unitofmeaure character varying,
    pressure_void character varying,
    sewerwatertype character varying,
    sewerwatertype_cl character varying,
    sewerwatertype_void character varying,
    id_pipe character varying,
    id_duct character varying,
    shape_length double precision
);

CREATE TABLE ussewerpipe_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    pipediameter_value character varying,
    pipediameter_unitofmeaure character varying,
    pipediameter_void character varying,
    pressure_value character varying,
    pressure_unitofmeaure character varying,
    pressure_void character varying,
    sewerwatertype character varying,
    sewerwatertype_cl character varying,
    sewerwatertype_void character varying,
    id_pipe character varying,
    id_duct character varying,
    shape_length double precision
);

CREATE TABLE usthermalpipe_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    pipediameter_value character varying,
    pipediameter_unitofmeaure character varying,
    pipediameter_void character varying,
    pressure_value character varying,
    pressure_unitofmeaure character varying,
    pressure_void character varying,
    thermalproducttype character varying,
    thermalproducttype_cl character varying,
    thermalproducttype_void character varying,
    id_pipe character varying,
    id_duct character varying,
    shape_length double precision
);

CREATE TABLE usthermalpipe_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900914),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    utilitydeliverytype character varying,
    utilitydeliverytype_cl character varying,
    utilitydeliverytype_void character varying,
    warningtype character varying,
    warningtype_cl character varying,
    warningtype_void character varying,
    pipediameter_value character varying,
    pipediameter_unitofmeaure character varying,
    pipediameter_void character varying,
    pressure_value character varying,
    pressure_unitofmeaure character varying,
    pressure_void character varying,
    thermalproducttype character varying,
    thermalproducttype_cl character varying,
    thermalproducttype_void character varying,
    id_pipe character varying,
    id_duct character varying,
    shape_length double precision
);

CREATE TABLE ustower_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900915),
    inspireid character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    towerheight double precision,
    towerheight_void character varying
);

CREATE TABLE ustower_utm26n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(Point,900914),
    inspireid character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    towerheight double precision,
    towerheight_void character varying
);

CREATE TABLE usutilitylink_utm25n (
    ogc_fid SERIAL PRIMARY KEY,
    wkb_geometry geometry(MultiLineString,900915),
    inspireid character varying,
    beginlifespanversion timestamp with time zone,
    beginlifespanversion_void character varying,
    endlifespanversion timestamp with time zone,
    endlifespanversion_void character varying,
    fictitious character varying,
    currentstatus character varying,
    currentstatus_cl character varying,
    currentstatus_void character varying,
    validfrom timestamp with time zone,
    validfrom_void character varying,
    validto timestamp with time zone,
    validto_void character varying,
    verticalposition character varying,
    verticalposition_cl character varying,
    verticalposition_void character varying,
    utilityfacilityreference_void character varying,
    governmentalservicereference_void character varying,
    shape_length double precision
);


